# Particle Physics Metadata Project

This is a space for collaboration - sharing files, meeting minutes and other documentation while working to improve data handling (mainly at the A4 Experiment) and related topics.

## Next Meeting

08:30 (CET) June 7th, 2024, [preliminary agenda](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/a4-metadata/-/blob/main/Minutes/20240607_minutes.md), zoom: to be announced by email, please contact gerrit.guenther@helmholtz-berlin.de to be added to the mailing list.

## Topics

Currently, we are discussing the following projects:
- **[A4 experiment](https://www.blogs.uni-mainz.de/fb08-ag-maas/a4-collaboration-at-mami/):** From experimental data to data publication (this repository).
- **[ALPS II](https://www.desy.de/forschung/anlagen__projekte/alps_ii/index_ger.html):** Conversion of experimental data to HDF5 adapting the [NeXus convention](https://manual.nexusformat.org/index.html) for Particle Physics experiments (no separate repository yet).
- **[Instrument PID](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/instrument_pid):** Creation of a file set to be uploaded to a globally available repository to define an instrumentation unambiguously through the minted DOI.
- **[ROOTmeta](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta):** Improving human and machine readability of [ROOT files](https://root.cern/).
- **[Hub Matter Metadata Schema](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/matter_metadata):** Development of a metadata schema for [Helmholtz Hub Matter](https://helmholtz-metadaten.de/en/matter/overview).
- **[Material Class Ontology (MCO)](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/mco):** Development of an ontology to categorize samples or the subject of investigation, e.g. for use in the [Hub Matter Metadata Schema](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/matter_metadata).

## Last Meeting
08:30 (CET) April 12th, 2024, see the [minutes](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/a4-metadata/-/blob/main/Minutes/20240412_minutes.md).