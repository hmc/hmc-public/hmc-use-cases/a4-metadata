# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

# content of file voltages_run78850.xml:
# <resource version="1.0" encoding="utf-8">
#     <head>
#         <date>2023-08-22T10:47:55.636557</date>
#     <generator>data_converter</generator>
#     </head>
#     <data>
#         <column1 about="#column1">
#             <name type="xsd:string" property="dc:title">channel number</name>
#             <units resource="https://doi.org/10.1351/goldbook.D01742" property="qudt:units">dimensionless
#                 <rel rel="owl:sameAs"/>
#             </units>
#             <comment type="xsd:string" property="dc:description">
#                 Numbering of detector unit (and, thus, related to detector position).
#             </comment>
#             <meanValue>511.5</meanValue>
#             <rangeBottom>1.0</rangeBottom>
#             <rangeTop>1022.0</rangeTop>
#             <dataset property="qudt:numericValue" type="xsd:integer">
#                 <value>1</value>
#                 <value>2</value>
#             </dataset>
#         </column1>
#         <column2 about="#column2">
#             <name type="xsd:string" property="dc:title">voltage</name>
#             <units typeOf="unit:Unit" resource="https://doi.org/10.1351/goldbook.V06634" property="qudt:units">Volt
#                 <abbrev property="qudt:abbreviation" type="xsd:string">V</abbrev>
#                 <rel rel="owl:sameAs" resource="qudt:Volt"/>
#             </units>
#             <comment type="xsd:string" property="dc:description">
#                 Electric potential applied to the photomultiplier (PMT) coupled to the central crystal of the channel.
#             </comment>
#             <meanValue>1250.3649706457925</meanValue>
#             <rangeBottom>987.0</rangeBottom>
#             <rangeTop>1500.0</rangeTop>
#             <dataset property="qudt:numericValue" type="xsd:integer">
#                 <value>1225</value>
#                 <value>1071</value>
#             </dataset>
#         </column2>
#     </data>
# </resource>

import os
import xml.etree.ElementTree as ET


class XMLReader(object):
    """
    Reads elements of a xml file.
    """

    @staticmethod
    def assemble_path(path_segments):
        """
        Joins a list of path elements by adding the system separator, e.g. '/' or '\'.
        :param path_segments: List of strings each containing a single path element (e.g. drive letter or folder),
        e.g. ['D:', 'a4-metadata', 'input_exampleData', 'xml_file_info', 'voltages_run78850.xml'].
        :return: String containing the assembled path,
        e.g. 'D:/a4-metadata/input_exampleData/xml_file_info/voltages_run78850.xml'.
        """
        target_path = ""
        first_segment = True
        for segment in path_segments:
            target_path = os.path.join(target_path, segment)
            if first_segment:
                # add additional separator if drive letter (e.g. 'D:')
                if segment.endswith(":"):
                    target_path = os.path.join(target_path, os.path.sep)
                first_segment = False

        return target_path

    def start_reading(self, path_list):
        """
        Reads the average value of each column from file voltages_run78850.xml (see above).
        :param path_list: List of strings each containing a path element leading to the xml file to be read,
        e.g. ['D:', 'a4-metadata', 'input_exampleData', 'xml_file_info', 'voltages_run78850.xml'].
        """
        xml_path = self.assemble_path(path_list)
        tree = ET.parse(xml_path)
        root = tree.getroot()
        for column in root.findall('data/*'):
            units = column.find("units")
            mean_value = column.find("meanValue")
            print("Average value of", column.tag + ":", mean_value.text, "in units of", units.text)

        pass


# standard entry point
if __name__ == "__main__":
    # define the path to the xml file to be read by giving separate path elements (including the xml file)
    xml_path_list = ["D:",
                     "a4-metadata",
                     "input_exampleData",
                     "xml_file_info",
                     "voltages_run78850.xml"]
    Reader = XMLReader()
    Reader.start_reading(xml_path_list)
    pass
