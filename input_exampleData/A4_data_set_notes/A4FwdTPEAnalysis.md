# The A4 TPE data analysis
This note briefly introduces the data analysis framework, which was used when analyzing the A4 data listed in the table below. These data were collocted in 2010 and 2011, mainly for two-photon-exchange(TPE) studies[^1].
The result of the beam transverse spin asymmetry in electron-proton elastic scattering has been published on [Phys. Rev. Lett. 124, 122003 (2020)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.124.122003).

| Beam time | GVZ |  rfrom----rto  | pol.  |# of valid runs | Beam energy |
|:---------:|:---:|:--------------:|:-----:|:--------------:|:-----------:|
| 191010    | 1   | 77320----77535 | 0.826 | 128            | 315.27      |
|           | 0   | 77540----77760 | 0.841 | 219            |             |
|           | 1   | 77773----77994 | 0.841 | 220            |             |
|           | 0   | 78000----78211 | 0.830 | 196            |             |
| 250111    | 0   | 80061----80100 | 0.848 |  39            | 315.13      |
|           | 1   | 80112----80151 | 0.836 |  39            |             |
|           | 0   | 80169----80246 | 0.836 |  77            |             |
|           | 1   | 80248----80393 | 0.836 | 145            |             |
|           | 0   | 80397----80436 | 0.836 |  39            |             |
| 260110    | 1   | 73835----74076 | 0.868 | 223            | 420.33      |
|           | 0   | 74077----74351 | 0.881 | 246            |             |
|           | 1   | 74352----74596 | 0.886 | 235            |             |
|           | 0   | 74597----74912 | 0.886 | 294            |             |
| 130410    | 0   | 74985----75495 | 0.853 | 375            | 510.29      |
|           | 1   | 75496----76206 | 0.886 | 667            |             |
| 191010    | 1   | 78842----79504 | 0.864 | 641            | 855.31      |
|           | 0   | 79505----80053 | 0.860 | 521            |             |
| 070910    | 0   | 76375----76807 | 0.802 | 428            | 1508.4      |
|           | 1   | 76810----77227 | 0.802 | 417            |             |
| 191010    | 0   | 78244----78633 | 0.846 | 376            | 1508        |
|           | 1   | 78641----78840 | 0.846 | 196            |             ||

## A4 raw data
The following chart shows a typical directroy structure of the A4 data, which are currently stored on the strangenix computers at KPH. 
As the names of the second tier directories indicate, data from different sub detectors/devices are stored sparately. 
The raw data were collected and saved in text format, they are stored in directories named "dataen". 
Shortly after the data taking, certain pre-analyses, such as differential nonlinear corrections, were performed over the raw data. 
The preanalyzed data are generally stored in directories "analyse". 
```
...
beam070910/
   pola/               ------------    data of beam polarization measurement 
      analyse/
      dataen/
   medusa/             ------------    data from the electromagnetic calorimeter (EMC) 
      analyse/
      dataen/
   lumi/               ------------    data from the luminosity monitor 
      analyse/
      dataen/
   camac/              ------------    I don't know what data are here, seems like target data 
      analyse/                         A self-explaining name would be helpful.
      dataen/
   target/             ------------    data about the liquid target operation status
      analyse/
      dataen/
   compton/            ------------    data from the Compton polarimetry
      analyse/
      dataen/
...
```
Generally, one needs information from both the raw and pre-analyzed data to perform physical analyses. 
Since various information are scattered in different files and in different formats, people other than the experts who know how the raw data were saved and preanalyzed would face a major difficulty in extracting the information they want. 
Sebastian and other colleagues have developed a set of useful ROOT functions in their early-day work. 
Thanks to these functions, nonexperts could get necessary information with less efforts.
As an example, three of the functions are shown below.

With the function <a name="GetParameter">**GetParameter**</a> one can get the value of any variable in a certain run by providing the relevant run number and parameter string. 
See an example of how it is called in the class [A4TreeWriter](#A4TreeWriter) show below.
```cpp–C++
Int_t GetParameter(Int_t runno,const char *parameter_string, Double_t *para, TFile *Summaryfile) 
{
  TRunSummary *RunSummary;
  char filename[256], name_string[256];
  Double_t value;

  if (!(Summaryfile->IsOpen())) {printf("error: could not open %s!\n",filename); return 1;}

  sprintf(name_string,"summary.%d",runno);
  RunSummary=(TRunSummary *)Summaryfile->Get(name_string);  
  if (RunSummary==NULL) {printf("Could not find %s!\n",name_string); return 1;}
  RunSummary->GetPara(parameter_string, &value);
  *para=value;
  
  delete RunSummary;
  return 0;
}
```

With the function <a name="ProjectX">**ProjectX**</a> one can build raw histograms out of the ADC data from a certain channel. 
See an example of how it is called in the class [A4TreeWriter](#A4TreeWriter) show below.
```cpp–C++
void TMedusa::ProjectX(TH1D **histpol0, TH1D **histpol1, Int_t ymin, Int_t ymax)
{
  char titel0[256], titel1[256], name0[256], name1[256];
  int x,y, index;
  Double_t sum0, sum1;

  sprintf(titel0,"%s pol0 ProjX",GetTitle());
  sprintf(titel1,"%s pol1 ProjX",GetTitle());
  sprintf(name0,"%s_p0_px",GetName());
  sprintf(name1,"%s_p1_px",GetName());
  
  if (ymax==64) {
    sprintf(name0,"%s_p0_px1",GetName());
    sprintf(name1,"%s_p1_px1",GetName());
  }
  if (*histpol0!=NULL) {delete *histpol0; *histpol0=NULL;}
  if (*histpol1!=NULL) {delete *histpol1; *histpol1=NULL;}
  *histpol0=new TH1D(name0,titel0,256,-0.5,255.5);
  *histpol1=new TH1D(name1,titel1,256,-0.5,255.5);
  if (ymin<0) ymin=0; if (ymax>63) ymax=63;
  for (x=0;x<256;x++) {
    sum0=0; sum1=0;
     for (y=ymin;y<=ymax;y++) {
       index=y*256+x; 
      	 sum0=sum0+memval[index]; 
	 sum1=sum1+memval[16384+index];
     }
     (*histpol0)->Fill(x,sum0);
     (*histpol1)->Fill(x,sum1);
  }
}
```

With the function <a name="CorrectDNL">**CorrectDNL**</a> one can correct for the ADC nonlinear effects, which are essential when fitting the EMC spectra. 
See an example of how it is called in the class [A4TreeWriter](#A4TreeWriter) show below.
```cpp–C++
int Tdnl::CorrectDNL(TH1D **cor0, TH1D **cor1, TH1D *raw0, TH1D *raw1, int chan,int runno) {
  int i, dnltab;
  Int_t binnumber;
  Double_t bincontent, currdnl;
  int dnlruns[2]={0,11172};
  char objname[256], title[256];
  
  if (runno<dnlruns[1]) dnltab=0; else dnltab=1;
  sprintf(objname,"%s dnlcorr",raw0->GetName());
  sprintf(title,"%s dnlcorr",raw0->GetTitle());

  if (*cor0!=NULL) {delete *cor0; *cor0=NULL;}
  *cor0=new TH1D(objname,title,256,-0.5,255.5);
  sprintf(objname,"%s dnlcorr",raw1->GetName());
  sprintf(title,"%s dnlcorr",raw1->GetTitle());
  if (*cor1!=NULL) {delete *cor1; *cor1=NULL;}
  *cor1=new TH1D(objname,title,256,-0.5,255.5);
  

  if (dnl[dnltab][chan][0]<0) return 1;
  for (i=0;i<256;i++) {
    currdnl=dnl[dnltab][chan][i];
    binnumber=raw0->FindBin(i);
    bincontent=raw0->GetBinContent(binnumber);
    if (currdnl>0) (*cor0)->SetBinContent(binnumber,bincontent/currdnl);
    binnumber=raw1->FindBin(i);
    bincontent=raw1->GetBinContent(binnumber);
    if (currdnl>0) (*cor1)->SetBinContent(binnumber,bincontent/currdnl);
  }
  return 0;
}


```


## A4 data in ROOT tree format
In order to make use of powerful funcationaties provided in the ROOT tree, e.g. quickly producing plots of arbitrary variable combinations, I decided to store A4 data in ROOT trees. 
So I wrote some classes derived from the ROOT TObject, here I list three classes which I think are most relevant to data conversion and storage being discussed right now. 
```
1. A4Run            - to store all information of a data-taking run which are needed in my analysis.
                        - general data such as beam parameters 
                        - a TClonesArray of 1022 A4Modules
                        - extendable on needs
2. A4Module         - to store spectra from a module of 3x3 crystal cluster.
3. A4TreeWriter     - to read the data from the raw and preanalyzed data and write them into A4Run 
                        - as soon as ROOT trees are created, they can be analyzed on computers other than the strangenix 
```
In the following I show parts of these class, so you get a feeling how they look like.


<a name="A4Run">**class A4Run**</a>
```cpp–C++
#ifndef ROOT_A4Run
#define ROOT_A4Run

...

class A4Run : public TObject {

...

private:

  ...

  //Beam
  Double_t          fBeamEnergy;
  Double_t          fBeamCurr;
  Double_t          fBeamCurrErr;
  Double_t          fBeamInteg;

  ...

  Int_t  _NumOfModules ;  // Number Of Modules
  TClonesArray *gModules ;   //->

  ...
  
public: 
  A4Run();
  virtual ~A4Run();

  ...

  Int_t               NumOfModules() const { return _NumOfModules ; }
  A4Module*           AddModule() ;
  A4Module*           GetModule(Int_t k) ; //k ranges from 0 to 1021
  void                ClearModules() ;

  ...

  ClassDef(A4Run,4)  //
};

#endif
```

<a name="A4Module">**class A4Module**</a>
```cpp–C++
#ifndef ROOT_A4Module
#define ROOT_A4Module

...

class A4Module : public TObject {

...

private:
  Int_t                 fModuleNr;      //module Nr. ranges from 1 to 1022
  Int_t                 M[3][3];        //modules in one 3x3 matrix
  Int_t                 fN0;       
  Int_t                 fN1;   
  Int_t                 fValley;
  Int_t                 fEdge;
  Int_t                 fPeak;
  Int_t                 fSigma;
  Double_t              fPionpos;
  Double_t              fDeltapos;

  Double_t          fHV;              //High voltage

  Double_t          fEpeak;         //energy of elastic scattered electron
  Double_t          fPionTh;        //pion threshold
  Double_t          fDeltaTh;      //delta threshold

  ...

private:
  
  ...

  Int_t  _NumOfHistos ;  // Number Of Histos with this module
  TClonesArray *gHistos ;   //->

  ...

public:
  A4Module(); 
  virtual ~A4Module();

  ...

  // Histo ...

  Int_t NumOfHistos() const { return _NumOfHistos ; }
  TH1D*  AddHisto() ;
  TH1D*  AddHisto(TH1D* hin) ;
  TH1D*  GetHisto(Int_t k) ;
  void  ClearHistos() ;

  ...

  //Num of events under the elastic peak
  Double_t               N0(Double_t msig, Double_t psig, Double_t leakcut = -111, Bool_t useDNL =  kFALSE);
  Double_t               N1(Double_t msig, Double_t psig, Double_t leakcut = -111, Bool_t useDNL =  kFALSE);
  Double_t               N0Err(Double_t msig, Double_t psig, Double_t leakcut = -111, Bool_t useDNL =  kFALSE);
  Double_t               N1Err(Double_t msig, Double_t psig, Double_t leakcut = -111, Bool_t useDNL =  kFALSE);

  ...

  ClassDef(A4Module,8)  //A4Module class
};

#endif
```

<a name="A4TreeWriter">**class A4TreeWriter**</a> ------------ [Back to GetParameter](#GetParameter) ------------ [Back to ProjectX](#ProjectX) ------------ [Back to CorrectDNL](#CorrectDNL)
```cpp–C++
void A4TreeWriter::WriteTree(){
  
    ...
    
    a4run->SetBeamEnergy(BeamEnerg->Energy(fRunNr));
    GetParameter(fRunNr, (char *)"G beam current", &Ibeam, sumfile);
    a4run->SetBeamCurr(Ibeam);
    GetParameter(fRunNr, (char *)"G beam current err", &IbeamErr, sumfile);
    a4run->SetBeamCurrErr(IbeamErr);
    GetParameter(fRunNr, (char *)"G beam integ", &IbeamItg, sumfile);
    a4run->SetBeamInteg(IbeamItg);
    GetParameter(fRunNr, (char *)"G mean HV", &HV, sumfile);
    a4run->SetMeanHV(HV);
    
    ...

    //add module
    //
    a4module = a4run->AddModule();
    a4module->SetModuleNr(mod);

    ...

    //Get raw spectra of 8-bit ADC and store them in p0[0] and p1[0]
    tmedusa->ProjectX(&p0[0],&p1[0],ymin, ymax);
    //correct DNL and store the corrected histogram in p0dnl[0] and p0dnl[1]
    DNL->CorrectDNL(&p0dnl[0], &p1dnl[0], p0[0], p1[0], mod, fRunNr);
 
    //Add histo
    a4module->AddHisto(p0[0]);
    a4module->AddHisto(p1[0]);
    a4module->AddHisto(p0dnl[0]);
    a4module->AddHisto(p1dnl[0]);

    ...

}
```

[^1]: The original purpose of the beam time *250111* was to investigate a moeller polarimeter using some water Cherenkov counters of the luminosity monitor. Since the EMC electronics were also switiched on, data from this beam time can be also used to study TPE. 
