# Notes from meeting 04 02 2022

## Updates

- access to electronic log book
   - copy log book to outside firewall
   - caveat is that it is read only
   - not available for A4, only available for new experiment P2
   - lots of similarities in terms of run start, run stop etc
   - elog is ASCII files
   - **to do** make copy and review
- reply from university library
  - we can use the repository
  - **to do** set up workflow here
- connection to PUNCH4NFDI
  - OM recommends that KS will ensure this
- directory of A4 data files
  - need to wait for description from Boxing
  - not designed for readability from outside world
- review metadata standards in particle physics
  - **to do** GG
- review of file formats in particle physics
   - is there a preferance in the team
   - team uses root files
   - need description of how data is organised within
   - (no fixed structure within the file)
   - 80-90% of information is stored in this format
- review repositories in particle physics
  - not required as university library provides repository
- quick intro to GitLab
   - hifis runs [introductory](https://events.hifis.net/category/4/) courses 
   - course catalogue from [hifis](https://www.helmholtz-hida.de/course-catalog) for Helmholtz association
   - next week spend 15 mins on the topic
