# Notes from meeting 24 06 2022

## Present
Malte, Gerrit, Oonagh (minutes)

## Updates

- discussion on Gerrit's [presentation](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/tree/master/docs)
- discussion on [mid-term report](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/blob/master/docs/A4-HMC-midterm.docx) Malte
- **to do** Gerrit collects material in repository and starts to gather together for overall solution
- Vivien will join next week with a few more questions on the landing page
