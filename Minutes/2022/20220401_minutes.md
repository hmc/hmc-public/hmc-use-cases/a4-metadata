# Notes from meeting 01 04 2022


## Present
Boxing, Gerrit, Vivien, Oonagh (minutes), Malte


## Updates

- short introduction to project for Vivien
   - Vivien is a student assistant in interface design who will work with Gerrit
   - three types of data
     - idea is to create [landing page](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/issues/9) (as for data publication) to get overview of what data is here and how the information fits together
     - for every xml file have corresponding html file so that it is human and machine readable
   - ASCII files
        - focus this week for Gerrit
           - opened second branch in repo to work on this
        - contains columns but not machine friendly
        - made converter to xml
        - need some information on content of ascii files (what do the columns stand for)
        - **to do** Gerrit sends to Boxing and Sebastian who will provide context
        - **open question** do all runs contain the same set of files
    - root files
      - **to do** compile list of questions about the information in these files
         - didn't start work on yet, want to understand the auxilary files (Elog and ASCII)
         - when work on auxilary files complete can make plan for the root files
    - elog book
         - xml file with some content you don't want to publish
         - Gerrit has made template to filter comments and machine content
         - question is how easy it is for the user to return content
         - need human-readable format along with xml (machine format)
         - Gerrit has written script, needs formatting work on css style sheet to make a nice pdf
         - **to do** Vivien looks at making style sheet for Elog

- idea to make [instrument PID](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/issues/10) 
   - contains information about experiment
   - removes doubt about where data comes from
   - new thing, need to discuss with library

- [input file folder](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/tree/master/input_exampleData/A4_data_set) (everthing reads in)
  - [output file folder](https://gitlab.hzdr.de/oonagh.mannix/a4-metadata/-/tree/master/output) (what is currently coming out)

-  link to P2 logbook due to refurbishment
  - we have other things to do at the moment; can wait until refurbishment is finished and elog comes back online

- changing configuration script from P2 to A4
   - on to-do list



