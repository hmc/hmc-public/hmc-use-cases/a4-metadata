# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import sys
from pathlib import Path


path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from converter.ascii_converter import *
from auxiliary.config_dict import *
from auxiliary.general_aux import *


class FileHandler(object):
    """
    Decides how files are processed (e.g. copied, converted, or modified).
    """

    def __init__(self):
        """
        Initializes class variables.
        """

        pass

    @staticmethod
    def process_file(subdata_path, parent_output_path, readme_dict, report_dict):
        """
        Starts file processing by analyzing the subdata_meta.json file and links the files to the run readme file.
        :param subdata_path: String containing the path to the folder containing the data set to be converted,
        e.g. 'D:/a4-metadata/input_exampleData/A4_data_set'.
        :param parent_output_path: String containing the path to the (parent) folder where the files of the data set
        should be written to, e.g. 'D:/a4-metadata/output'.
        :param readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the summary of
        the data set to be written to the readme file at the sub-directory level of the dataset; will contain an entry
        for every file within the data set.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :return: String containing the description of the data set to be written to the top-level readme file.
        """
        subdata_dict = ConfigDict()
        subdata_dict.read_json(subdata_meta_path())

        run_description = ""

        # loop over all files defined in config_dict
        files_dict = subdata_dict.get_value("files")
        for _, _dict in files_dict.items():
            file_dict = ConfigDict()
            file_dict.read_dict(_dict)
            subfolder_path = os.path.join(subdata_path, file_dict.assemble_path("relative_path"))

            # create sub-folder in output path of same name as input folder
            output_path = os.path.join(
                parent_output_path, subdata_path.rsplit(os.path.sep, 1)[1]
            )
            if "output_path" in file_dict.get_content():
                # create folders of output path for current file if required (maybe in a child folder)
                for folder in file_dict.get_value("output_path", output_path):
                    output_path = os.path.join(output_path, folder)
                    create_path(output_path)

            if "filename_part" in file_dict.get_content():
                # try to find the file by comparing with part of the file name
                raw_file_found, raw_filename = guess_file(
                    subfolder_path, file_dict.get_value("filename_part", subfolder_path)
                )
                raw_file_path = os.path.join(subfolder_path, raw_filename)
                if raw_file_found:
                    # check if a customized class is defined to handle file
                    if "useCustomClass" in file_dict.get_keys():
                        # create summary dictionary of _file_dict and _config_dict
                        _sum_dict = ConfigDict()
                        _sum_dict.add_value("file_dict", file_dict.get_content())
                        _sum_dict.add_value("config_dict", subdata_dict.get_content())
                        _sum_dict.add_value("subdata_path", subdata_path)
                        _sum_dict.add_value("output_path", output_path)
                        _sum_dict.add_value("parent_output_path", parent_output_path)
                        _sum_dict.add_value("filename", raw_filename)

                        # use get_value routine to start customized class
                        file_dict.get_value("useCustomClass", subfolder_path, readme_dict,
                                            _sum_dict)

                    # otherwise, use a standard routine
                    else:
                        _file_format = file_dict.get_value("file_format")
                        if _file_format == "ASCII":
                            # start ASCII converter
                            converter = ASCIIConverter()
                            converter.handle_file(raw_file_path, parent_output_path,
                                                  output_path, file_dict, subdata_dict, readme_dict, report_dict)
                            del converter
                        else:
                            # just copy original file
                            raw_file_output_path = os.path.join(output_path, raw_filename)
                            shutil.copy2(raw_file_path, raw_file_output_path)
                            readme_dict.add_file(raw_filename, file_dict, subfolder_path, False)

                    run_description = subdata_dict.get_value("dataset_description", subfolder_path)

            del file_dict

        del subdata_dict

        return run_description

    @staticmethod
    def add_supplement(top_readme_dict, project_dict):
        """
        Adds a single file to the output path and links it to the top readme file, e.g. a pdf describing the
        organization and content of files within a data set.
        :param top_readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information
        of the top readme file, e.g. links to the data sets and supplement files.
        :param project_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information
        about the project, e.g. a list of supplement files to be included.
        :return: None.
        """
        files_dict = ConfigDict()
        files_dict.read_dict(project_dict.get_value("files"))

        # process supplement files that are added to the top readme file
        for _file, file_dict in files_dict.get_items():
            supplement_dict = ConfigDict()
            supplement_dict.read_dict(file_dict)
            raw_filename = supplement_dict.get_value("filename_part")

            raw_file_path = os.path.join(supplement_dict.assemble_path("relative_path"), raw_filename)

            output_path = os.path.join(project_dict.assemble_path("output_path"), supplement_dict.assemble_path("output_path"))
            raw_file_output_path = os.path.join(output_path, raw_filename)
            shutil.copy2(raw_file_path, raw_file_output_path)
            top_readme_dict.add_file(raw_filename, supplement_dict, "dummy", False)

        pass
