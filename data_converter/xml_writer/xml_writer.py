# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

import datetime
import os
import pytz
import shutil
import sys
from pathlib import Path
# from xml.etree import ElementTree as ET

import numpy as np
from lxml import etree as ET

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.config_dict import *
from auxiliary.general_aux import *


class XMLWriter(object):
    """
    Writes a xml file.
    """

    def __init__(self, _root_name, _file_id="", _version="1.0", data_cite=False):
        """
        Initializes the basic element of the ET object, i.e. the 'root' level.
        :param _root_name: String containing the name of the root section, e.g. "Dataset".
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :param _version: String containing the name of the version; optional, default value is "1.0".
        :param data_cite: Boolean that is 'True' if the DataCite schema should be added as namespace; otherwise
        the namespaces of rdf definitions are added; optional, default value is "False".
        """
        self.file_id = _file_id

        # read config_project/project_meta.json config file
        self.project_dict = ConfigDict()
        self.project_dict.read_json(project_meta_path())

        # variable, e.g. to be used in ASCII-to-xml conversion
        self.definitions = ConfigDict()
        self.definitions.read_dict(self.collect_definitions(True))

        # default namespaces
        self.xml_ns = "http://www.w3.org/XML/1998/namespace"
        self.prov_ns = "http://www.w3.org/ns/prov#"
        # self.xsi_ns = "http://www.w3.org/2001/XMLSchema-instance"
        self.dc_ns = "http://purl.org/dc/elements/1.1/"

        # register all namespaces (although not all might be used)
        _nsmap = self.collect_definitions(False)
        _nsmap["xml"] = self.xml_ns
        _nsmap["prov"] = self.prov_ns
        # _nsmap["xsi"] = self.xsi_ns
        _nsmap["dc"] = self.dc_ns

        if data_cite:
            # Create root level of ET object; no whitespaces allowed in _root_name
            qname = ET.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
            self.xml_root = ET.Element(_root_name.replace(" ", "_"), {
                qname: "http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4.1/metadata.xsd"},
                            nsmap=_nsmap)
        else:
            # Create root level of ET object; no whitespaces allowed in _root_name
            self.xml_root = ET.Element(_root_name.replace(" ", "_"), version=_version, encoding="utf-8", nsmap=_nsmap)

        pass

    def collect_definitions(self, _full_dict=False):
        """
        Collects namespaces and prefixes from config files such as 'definition_global.json', 'project_meta.json', and
        'subdata_meta.json'. Please note the hierarchy in overwriting definitions.
        :param _full_dict: Boolean that is 'True' if all content of 'definitions' is collected 'as it is'; otherwise
        the returned dictionary contains only prefix-namespace as key-value pairs; optional.
        :return: Dictionary.
        """
        _def_dict = {}

        # check if global definitions exist (in a file of name 'definition_global.json')
        _def_file = "definition_global.json"
        if find_file(config_project_path(), _def_file):
            _global_dict = ConfigDict()
            _global_dict.read_json(os.path.join(config_project_path(), _def_file))
            _def_dict.update(self.crawl_definitions(_global_dict, True, _full_dict))

        _subdata_dict = ConfigDict()
        _subdata_dict.read_json(subdata_meta_path())
        _def_dict.update(self.crawl_definitions(_subdata_dict, False, _full_dict))

        _def_dict.update(self.crawl_definitions(self.project_dict, False, _full_dict))

        # check if file specific definitions exist (in a file named definitions_[filename].json)
        _def_file = "definition_" + self.file_id.split(".")[0] + ".json"
        if find_file(config_project_path(), _def_file):
            _specific_dict = ConfigDict()
            _specific_dict.read_json(os.path.join(config_project_path(), _def_file))
            _def_dict.update(self.crawl_definitions(_specific_dict, True, _full_dict))

        return _def_dict

    @staticmethod
    def crawl_definitions(_main_dict, _file_dict, _full_dict):
        """
        Collects prefixes and associated namespaces from the "definitions" sub-dictionary included in the given
        '_main' dictionary or adds the entire "definitions" section as it is (if _full_dict is 'True').
        :param _main_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing a sub-dictionary
        associated to the key 'definitions', e.g. as read from project_meta.json and subdata_meta.json file.
        :param _file_dict: Boolean that is 'True' if _main_dict is read from a file-specific definitions file
        (where definitions are given at top-level); otherwise definitions are expected to be in a sub-dictionary of key
        'definitions'.
        :param _full_dict: Boolean that is 'True' if all content of 'definitions' is collected 'as it is'; otherwise
        the returned dictionary contains prefix-namespace as key-value pairs.
        :return: Dictionary.
        """
        _def_dict = {}
        _definitions = {}

        if _file_dict:
            # file-specific definitions
            _definitions = _main_dict.get_content()
        else:
            # default definitions in sub-dictionary "definitions" of e.g. project_meta.json or subdata_meta.json
            _defkey = "definition"
            if _defkey in _main_dict.get_keys():
                _definitions = _main_dict.get_value(_defkey)

        for _item, _dict in _definitions.items():
            if _full_dict:
                _def_dict[_item] = _definitions[_item]
            else:
                for _sub_item, _sub_dict in _dict.items():
                    if "namespace" in _sub_dict and "namespace_prefix" in _sub_dict:
                        _def_dict[_sub_dict["namespace_prefix"]] = _sub_dict["namespace"]
                    if "attr_namespace" in _sub_dict and "attr_namespace_prefix" in _sub_dict:
                        _def_dict[_sub_dict["attr_namespace_prefix"]] = _sub_dict["attr_namespace"]

        return _def_dict

    def create_header(self, _header_name="head", _generator="data_converter"):
        """
        Adds a header section to the ET object.
        :param _header_name: String containing the name of the header section; optional, default value is "header".
        :param _generator: String containing the name of the program used to create the ET object (this code);
        optional, default value is "data_converter".
        :return: None.
        """
        # no whitespaces allowed in _header_name
        header = ET.SubElement(self.xml_root, _header_name.replace(" ", "_"))
        ET.SubElement(header, "date").text = datetime.datetime.utcnow().isoformat()
        ET.SubElement(header, "generator").text = _generator

        pass

    def write_xml(self, filename, _output_path):
        """
        Writes an ET object as xml file.
        :param filename: String containing the name of the file that should be written, e.g. 'Foersterlog.xml'.
        :param _output_path: String containing the path to the output folder where the file should be written,
        e.g. 'D:/a4-metadata/output/A4_data_set/run78850/camac/daten'.
        :return: None.
        """
        # Remove unused namespaces; seems to delete all namespaces (does it search sub-root elements?)
        # ET.cleanup_namespaces(self.xml_root)

        # Write ET to xml file:
        xml_path = os.path.join(_output_path, filename)
        ET.ElementTree(self.xml_root).write(
            xml_path, encoding="utf-8", xml_declaration=True
         )

        pass

    def xml2html(self, _xml_path, _xsl_path):
        """
        Reads a xml file and converts it into pdf file by applying a formatting provided by a xslt style sheet.
        :param _xml_path: String containing the path to the xml file that should be converted into pdf.
        :param _xsl_path: String containing the path to the xsl file that should be used to format the xml file.
        :return: String containing the name of the produced html file.
        """
        # parse xml to html using style sheet xslt
        xslt_doc = ET.parse(_xsl_path)
        xslt_transformer = ET.XSLT(xslt_doc)

        source_doc = ET.parse(_xml_path)
        output_doc = xslt_transformer(source_doc)

        output_html_path = _xml_path[:-4] + ".html"
        output_doc.write(output_html_path, pretty_print=True)

        # write html to pdf using wkhtmltopdf
        # output_pdf_path = _xml_path[:-4] + '.pdf'
        # config = pdfkit.configuration(wkhtmltopdf='C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe')
        # pdfkit.from_file(output_html_path, output_pdf_path, configuration=config)

        # mimicking that a URL to a picture is given in the xml pointing to a picture (e.g. plot)
        # url = 'https://www.helmholtz-berlin.de/media/media/forschung/wi/optik-strahlrohre/aquarius/' \
        #       'usecasefluxresolutioncff2p25sv0p015.png'
        # self.add_webpic2pdf(url, output_pdf_path)

        html_filename = output_html_path.rsplit(os.path.sep, 1)[-1]

        return html_filename

    def array2xml(self, array_data, file_dict, _current_dir, report_dict):
        """
        Writes an array to a single xml file by creating a separate data set for each column.
        :param array_data: Numpy array.
        :param file_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing a (sub-)dictionary
        for a file as read from 'config_project/subdata_meta.json', e.g. for 'Foersterlog' it is {'relative_path':
        ['camac', 'daten'], 'filename_part': 'Foersterlog', 'content_type': 'table', 'header': {'col 1': ... }}
        :param _current_dir: String containing the path to the current_directory.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :return: None. Writes xml file directly.
        """
        # read available definitions
        # self.definitions.read_dict(self.collect_definitions(True))

        # Create data sub-element:
        data = ET.SubElement(self.xml_root, "data")

        # here, "header" corresponds to the column header (e.g. containing names of columns),
        # (not the xml header section)
        header_dict = file_dict.get_value("header", _current_dir)
        file_column = 0
        column_no = 0
        table_no = 0
        for _ in range(0, len(header_dict)):
            column_name = "col " + str(column_no)
            # check if column is sub-table:
            if column_name in header_dict:
                column_dict = ConfigDict()
                column_dict.read_dict(header_dict[column_name])
                if "header" in header_dict[column_name]:
                    # if another header section is present then column is a sub-table comprising a set of sub-columns
                    table_no += 1
                    sub_table = self.add_sub_table(table_no, column_dict, data, _current_dir)

                    sub_header_dict = header_dict[column_name]["header"]
                    ignored_cols = 0
                    for sub_col_no in range(0, len(sub_header_dict)):
                        sub_col_name = "col " + str(sub_col_no)
                        if sub_col_name in sub_header_dict:
                            sub_col_dict = sub_header_dict[sub_col_name]
                            if "use_col" in sub_col_dict:
                                # use this column instead if >0
                                # otherwise, skip line without adding
                                use_column = sub_col_dict["use_col"]
                                if use_column > -1:
                                    # use another column dictionary
                                    self.column2xml(use_column, array_data, sub_col_dict, sub_table, _current_dir,
                                                    report_dict, "column" + str(sub_col_no+1-ignored_cols))
                                else:
                                    ignored_cols += 1
                            else:
                                self.column2xml(file_column, array_data, sub_col_dict, sub_table, _current_dir,
                                                report_dict, "column" + str(sub_col_no + 1 - ignored_cols))
                            file_column += 1

                else:
                    # otherwise, add column
                    _sub_dict = file_dict.get_value("header", _current_dir)[column_name]
                    self.column2xml(file_column, array_data, _sub_dict, data, _current_dir, report_dict)
                    file_column += 1
                column_no += 1

        pass

    def add_sub_table(self, table_no, column_dict, data, _current_dir):
        """
        Adds a sub-table element comprising a number of columns as defined in the corresponding 'header' section of the
        file dictionary.
        :param table_no: Integer containing the number of the sub-table within the file.
        :param column_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing a column
        section (a sub-dictionary) for a file, e.g. as read from 'config_project/subdata_meta.json'; here, the column
        comprises a sub-table with sub-columns.
        :param data: Etree handle to the section of the xml file (e.g. 'data' element) where the sub-table should be
        added.
        :param _current_dir: String containing the path to the current_directory.
        :return: Etree handle (of the created sub-table element).
        """
        sub_table = ET.SubElement(data, "table" + str(table_no))
        if "name" in column_dict.get_keys():
            self.add_element(sub_table, "name", column_dict, _current_dir)
        if "comment" in column_dict.get_keys():
            self.add_element(sub_table, "comment", column_dict, _current_dir)

        return sub_table

    def column2xml(self, column_no, array_data, _sub_dict, data_element, _current_dir, report_dict, column_name=""):
        """
        Adds a column (defined as 'col 0', 'col 1', 'col 2'... in e.g. subdata_meta.json) to the xml file.
        :param column_no: Integer containing the column number of the original file to be added.
        :param array_data: Numpy array (2d) containing the file content as a table of rows and columns.
        :param _sub_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing a sub-dictionary of
        the files 'header' section which defines the column (e.g. 'name' and 'comment').
        :param data_element: Etree handle to the section of the xml file (e.g. 'data' element) where the column
        should be added.
        :param _current_dir: String containing the path to the current_directory.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :param column_name: String containing the name of the element that will contain the column, e.g. 'column1'.
        :return: None.
        """
        # check if the column_no is within the column-range of the array
        if len(array_data) > column_no:
            if column_name == "":
                column_name = "column" + str(column_no + 1)

            column_dict = ConfigDict()
            column_dict.read_dict(_sub_dict)

            column = ET.SubElement(data_element, column_name)
            if "name" in column_dict.get_keys():
                self.add_element(column, "name", column_dict, _current_dir, "", report_dict)
            _units_element = self.add_element(column, "units", column_dict, _current_dir, "", report_dict)
            self.add_element(column, "comment", column_dict, _current_dir, "", report_dict)

            data_type = column_dict.get_value("data_type", _current_dir)
            self.add_array(column, array_data[column_no], data_type)

        pass

    def add_root_element(self, _element):
        """
        Creates an Etree class element at the xml root file level.
        :param _element: String containing the name of the element that should be created at the xml root file level,
        e.g. 'data'.
        :return: ET handle (of the created element).
        """
        return ET.SubElement(self.xml_root, _element)

    @staticmethod
    def create_node(parent_element, node_name):
        """
        Adds an empty element to the Etree class object representing the xml file.
        :param parent_element: Handle of an Etree object element where the element should be added.
        :param node_name: String containing the name of the element to be created.
        :return: Etree handle (of the created element).
        """
        _node = ET.SubElement(parent_element, node_name)
        return _node

    def add_element(self, _sub_element, _key, _dict, _current_dir, _element_name="", report_dict=None):
        """
        Adds an element to the Etree (xml) handle if the corresponding key (_key) exists in the dictionary (_dict). If
        an (optional) element name is defined, the element's name may differ from the key's name, otherwise, the key
        name is used to name the element.
        :param _sub_element: Python handle of a sub-element of the Etree object.
        :param _key: String containing the key of the dictionary _dict to be read, e.g. 'units'; the key is also used to
        name the added xml element no _element_name is provided.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs providing
        information about the element to be added (e.g. about a person or a file).
        :param _current_dir: String containing the path to the current_directory.
        :param _element_name: String containing the name of the element; optional. If none is given the key name is used
        to name the element.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process; optional.
        :return: None type object.
        """
        # use the key's name if name is not defined explicitly
        if _element_name == "":
            _element_name = _key

        # add element
        if _key in _dict.get_content():

            # only add non-empty elements
            add = False
            _value = _dict.get_value(_key, _current_dir)
            if not isinstance(_value, list):
                if not _value.strip() == "":
                    add = True
            else:
                if len(_value) > 0:
                    add = True

            if add:
                # check if definitions for the element are given
                def_found = False

                if len(_value) > 1:
                    # use case-insensitive comparison if string consists of more than 1 char
                    if _value.lower() in self.definitions.get_lowercase_keys():
                        def_found = True
                else:
                    # use case-sensitive comparison if string consists of 1 character, e.g. to distinguish voltage (V)
                    # and velocity (v)
                    if _value in self.definitions.get_keys():
                        def_found = True

                if def_found:
                    # add definition and content if definitions are found
                    _key_def_dict = ConfigDict()
                    _key_def_dict.read_dict(self.definitions.get_value(_value))
                    _sub_element = self.add_defined_element(_sub_element, _element_name, _value, _dict, _key_def_dict)
                else:
                    # otherwise, add content and make report entry (if report handle exist)
                    _sub_element = self.add_value(_sub_element, _element_name, _value)
                    if isinstance(report_dict, type("ReportLog")):
                        report_dict.add_missing_resource(_element_name, _value, _current_dir, self.file_id)



        return _sub_element

    def add_defined_element(self, _sub_element, _element_name, _text_value, _dict, _key_def_dict):
        """
        Adds an element with additional annotations derived from the "definitions" made in project_meta.json,
        subdata_meta.json, definition_global.json, and definition[filename].json, e.g. including namespace convention
        such as "<units qudt:property="Number" owl:rel="sameAs">counts</units>".
        :param _sub_element: Python handle of a sub-element of the Etree object.
        :param _element_name: String containing the name of the element to be created.
        :param _text_value: String containing the content of the element to be created.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs providing
        information about the element to be added (e.g. about a person or a file).
        :param _key_def_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        of the attributes to be added to the created element (where values are dictionaries, too).
        :return: Handle to the created ET element.
        """
        def_found = False
        element_created = False
        for _key, _dict in _key_def_dict.get_items():
            _value = _dict["value"]

            # create element only once
            if not element_created:
                element = ET.SubElement(_sub_element, _element_name)
                element_created = True

            if _element_name == "units":
                if _key == "typeOf":
                    def_added = self.add_defined_attribute(_key, _value, _dict, element)
                    if def_added:
                        def_found = True

                if _key == "resource":
                    def_added = self.add_defined_attribute("resource", _value, _dict, element)
                    if def_added:
                        def_found = True

                if _key == "abbreviation":
                    # element.text = _value
                    # element.set("type", "xsd:string")

                    abbrev_handle = self.make_element(element, "abbrev")
                    def_added = self.add_defined_attribute("property", "abbreviation", _dict, abbrev_handle)
                    if def_added:
                        def_found = True
                    abbrev_handle.set("type", "xsd:string")
                    abbrev_handle.text = _value

                if _key == "rel":
                    rel_handle = self.make_element(element, "rel")
                    def_added = self.add_defined_attribute("rel", _value, _dict, rel_handle)
                    if def_added:
                        def_found = True

                if _key == "rel_resource":
                    rel_handle = self.make_element(element, "rel")
                    def_added = self.add_defined_attribute("resource", _value, _dict, rel_handle)
                    if def_added:
                        def_found = True

            else:
                if "namespace" in _dict and "namespace_prefix" in _dict:
                    # add with given namespace
                    # _prefix = _dict["namespace_prefix"]
                    _namespace = _dict["namespace"]
                    if "attr_namespace" in _dict and "attr_namespace_prefix" in _dict:
                        _attr_namespace = _dict["attr_namespace"]
                        element.set(ET.QName(_attr_namespace, _key), ET.QName(_namespace, _value))
                    else:
                        element.set(_key, ET.QName(_namespace, _value))
                    def_found = True
                else:
                    # add without namespace, e.g. resource with url
                    element.set(_key, _value)

        if def_found:
            sub_name = _sub_element.tag
            _sub_element.set("about", "#"+sub_name)
            element.text = str(_text_value)
        else:
            element = self.add_value(_sub_element, _element_name, _value)

        self.add_properties(element, _element_name)

        return element

    def add_properties(self, element_handle, element_name):
        """
        Adds default properties to default elements such as "name" or "units".
        """
        if element_name == "name":
            element_handle.set("property", ET.QName(self.dc_ns, "title"))
        elif element_name == "comment":
            element_handle.set("property", ET.QName(self.dc_ns, "description"))
        elif element_name == "units":
            element_handle.set("property", ET.QName("http://qudt.org/2.1/vocab/unit#", "units"))

        pass

    @staticmethod
    def add_defined_attribute(_key, _value, _dict, element):
        def_added = False
        if "namespace" in _dict and "namespace_prefix" in _dict:
            # add with given namespace
            # _prefix = _dict["namespace_prefix"]
            _namespace = _dict["namespace"]
            if "attr_namespace" in _dict and "attr_namespace_prefix" in _dict:
                _attr_namespace = _dict["attr_namespace"]
                element.set(ET.QName(_attr_namespace, _key), ET.QName(_namespace, _value))
            else:
                element.set(_key, ET.QName(_namespace, _value))
            def_added = True
        else:
            # add without namespace, e.g. resource with url
            element.set(_key, _value)

        return def_added

    @staticmethod
    def make_element(parent_handle, element_name):

        if len(parent_handle.findall(element_name, namespaces=None)) == 0:
            abbrev_handle = ET.SubElement(parent_handle, element_name)
            # print(" ** not found")
        else:
            abbrev_handle = parent_handle.findall(element_name, namespaces=None)[0]
            # print(" ** found")

        return abbrev_handle








    def add_array(self, _sub_element, _column, _data_type):
        """
        Adds an array as a separate 'dataset' sub-group to a xml file.
        :param _sub_element: Python handle to the sub-element of the ET object where the array should be added.
        :param _column: Numpy array containing the values of a column.
        :param _data_type: String defining the type of column data, e.g. "integer".
        :return: None. (Adds content to the xml file directly.)
        """
        _column, _datatype = self.array_str2float(_column, _data_type)
        if _data_type == "integer" or _data_type == "float":
            ET.SubElement(_sub_element, "meanValue").text = str(np.mean(_column))
            ET.SubElement(_sub_element, "rangeBottom").text = str(min(_column))
            ET.SubElement(_sub_element, "rangeTop").text = str(max(_column))
        sub = ET.SubElement(_sub_element, "dataset")
        sub.set("property", ET.QName("http://qudt.org/2.1/vocab/unit#", "numericValue"))
        for i in range(len(_column)):
            self.add_array_value(sub, "value", _column[i], _data_type)

        pass

    @staticmethod
    def array_str2float(_column, _data_type):
        """
        Tries to convert a numpy array of strings to a numpy array of floats.
        :param _column: Numpy array of any type, e.g. 'np.int' or 'np.str'.
        :param _data_type: String containing an identifier for the type of the data, e.g. 'integer' or 'float'.
        :return: Array (which can be of any type but, if possible, is converted to float).
        """
        if isinstance(_column[0], np.str):
            try:
                _column = _column.astype(np.float)
            except:
                _data_type = "string"
                pass

        return _column, _data_type

    @staticmethod
    def get_type(_value):
        """
        Tries to convert the passed value in various types and, if successful, returns the variable type.
        :param _value: Variable of any type, e.g. string, integer float, numpy array...
        :return: String (containing the determined type of variable).
        """
        # check if _value is numeric
        try:
            _dummy = float(_value)
            if str(_value).count(".") == 1:
                _data_type = "float"
            else:
                _data_type = "integer"
        except:
            _data_type = "string"
            pass

        # check if _value of type string is utc date
        if _data_type == "string":
            if str(_value).count("-") == 3:
                _value = str(_value).rsplit("-", 1)[0]
            elif "+" in str(_value):
                _value = str(_value).rsplit("+", 1)[0]
            format_utc = "%Y-%m-%dT%H:%M:%S"
            try:
                date = datetime.datetime.strptime(str(_value), format_utc)
                _data_type = "datetime"
            except ValueError:
                pass

            format_time = "%H:%M:%S"
            try:
                date = datetime.datetime.strptime(str(_value), format_time)
                _data_type = "time"
            except ValueError:
                pass

        return _data_type

    def add_value(self, _et_element, _value_name, _value, _data_type="", create_element=True):
        """
        Adds a value (as text string) as part of a list to a sub-element of an ET object; in contrast to the
        add_array_value() routine, the _data_type is assigned to the added element.
        :param _et_element: Python handle to the sub-element of the ET object.
        :param _value_name: String containing the name of the element that is created to include the _value,
        e.g. "value".
        :param _value: Float or integer to be written as text string to the sub-element, e.g. '1'.
        :param _data_type: String defining the type of column data, e.g. "integer".
        :param create_element: Boolean that is 'True' if a sub-element should be created to contain the value.
        :return: None.
        """
        # try to identify data type of none is given
        if _data_type == "":
            _data_type = self.get_type(_value)

        if create_element:
            _et_element = ET.SubElement(_et_element, _value_name)
        if _data_type == "integer":
            _et_element.text = str(int(_value))
            _et_element.set("type", "xsd:integer")
        elif _data_type == "float":
            _et_element.text = str(_value)
            _et_element.set("type", "xsd:float")
        elif _data_type == "datetime":
            _et_element.text = str(_value)
            _et_element.set("type", "xs:dateTime")
        elif _data_type == "time":
            _et_element.text = str(_value)
            _et_element.set("type", "xs:time")
        else:
            _et_element.text = str(_value)
            _et_element.set("type", "xsd:string")

        self.add_properties(_et_element, _value_name)

        return _et_element

    def add_array_value(self, _sub_element, _value_name, _value, _data_type=""):
        """
        Adds a value (as text string) as part of a list to a sub-element of an ET object; in contrast to the
        add_value() routine, the _data_type is assigned to the top-level element of added data.
        :param _sub_element: Python handle to the sub-element of the ET object.
        :param _value_name: String containing the name of the element that is created to include the _value,
        e.g. "value".
        :param _value: Float or integer to be written as text string to the sub-element, e.g. '1'.
        :param _data_type: String defining the type of column data, e.g. "integer".
        :return: None.
        """
        # try to identify data type of none is given
        if _data_type == "":
            _data_type = self.get_type(_value)

        if _data_type == "integer":
            ET.SubElement(_sub_element, _value_name).text = str(int(_value))
            _sub_element.set("type", "xsd:integer")
        elif _data_type == "float":
            ET.SubElement(_sub_element, _value_name).text = str(_value)
            _sub_element.set("type", "xsd:float")
        elif _data_type == "datetime":
            ET.SubElement(_sub_element, _value_name).text = str(_value)
            _sub_element.set("type", "xs:dateTime")
        else:
            ET.SubElement(_sub_element, _value_name).text = str(_value)
            _sub_element.set("type", "xsd:string")

        pass

    def add_file(self, _sub_element, _dataset_dict, _current_dir):
        """
        Adds a link to an ET object.
        :param _sub_element: Python handle to the xml sub-group where the array should be added.
        :param _dataset_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the information
        about a sub-dataset (e.g. of the file 'info.78850'), e.g. {'filename': 'info_78850.xml',
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        required_list = ["name", "output_path"]
        if all(key in _dataset_dict.get_content() for key in required_list):
            _link = ET.SubElement(_sub_element, "a")

            _path_list = _dataset_dict.get_value("output_path", _current_dir)
            _file_path = os.path.join(assemble_path("", _path_list), _dataset_dict.get_value("name", _current_dir))
            _link.text = _file_path

            if "primarySource" in _dataset_dict.get_content():
                if not _dataset_dict.get_value("primarySource", _current_dir) == "":
                    _link.set(ET.QName(self.prov_ns, "hadPrimarySource"),
                              _dataset_dict.get_value("primarySource", _current_dir))
            if "generatedBy" in _dataset_dict.get_content():
                if not _dataset_dict.get_value("generatedBy", _current_dir) == "":
                    _link.set(ET.QName(self.prov_ns, "generatedBy"),
                              _dataset_dict.get_value("generatedBy", _current_dir))

            _link.set("href", _file_path)
            if "description" in _dataset_dict.get_content():
                _link.set("description", _dataset_dict.get_value("description", _current_dir))
            else:
                _link.set("description", "")
            if "footnote" in _dataset_dict.get_content():
                _link.set("footnote", _dataset_dict.get_value("footnote", _current_dir))

        pass

    @staticmethod
    def add_attribute(_sub_element, _key, _dict, _current_dir):
        """
        Adds an attribute to a sub-element of an ET object by using the key as the attribute's name.
        :param _sub_element: Python handle to an ET object.
        :param _key: String containing the key (_key) of the dictionary (_dict) whose value should be added;
        the key name defines also the name of the attribute, e.g. 'nameIdentifierScheme'.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs whose
        values should be added.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if _key in _dict.get_content():
            _sub_element.set(_key, _dict.get_value(_key, _current_dir))
        pass

    @staticmethod
    def add_renamed_attribute(_sub_element, _key, _dict, _renamed_key, _current_dir):
        """
        Adds an attribute to a sub-element of an ET object by choosing the name of the attribute.
        :param _sub_element: Python handle to an ET object.
        :param _key: String containing the key (_key) of the dictionary (_dict) whose value should be added.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs whose
        values should be added.
        :param _renamed_key: String containing the name of the attribute which should contain the value of _dict[_key];
        the key name may differ from the name of the attribute, e.g. the value of key 'rights_url' is assigned as an
        attribute 'href'.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if _key in _dict.get_content():
            _sub_element.set(_renamed_key, _dict.get_value(_key, _current_dir))
        pass

    def add_person(self, _parent_group, _element_name, _person_dict, _current_dir):
        """
        Adds information of a person (as used for creator or contributor) to an ET object according to the dataCite
        schema (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _parent_group: Python handle to an element of an ET object of which the current person should become a
        sub-element.
        :param _element_name: String containing the name of the element to which the personal details should be added; e.g.
        "creator" or "contributor".
        :param _person_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        providing information about a person.
        :param _current_dir: String containing the path to the current directory.
        :return:
        """
        creator_name = "creatorName"
        _person = ET.SubElement(_parent_group, _element_name)
        _required_list = ["givenName", "familyName"]
        if _element_name == "contributor":
            _required_list.append("contributorType")
        if all(key in _person_dict.get_content() for key in _required_list):
            _sub_dict = {
                creator_name: _person_dict.get_value("familyName", _current_dir) + ", "
                + _person_dict.get_value("givenName", _current_dir),
                "nameType": "Personal",
            }
            _dummy_dict = ConfigDict()
            _dummy_dict.read_dict(_sub_dict)
            name = self.add_element(_person, creator_name, _dummy_dict, _current_dir)
            self.add_attribute(name, "nameType", _dummy_dict, _current_dir)

            self.add_element(_person, "givenName", _person_dict, _current_dir)
            self.add_element(_person, "familyName", _person_dict, _current_dir)

            # contributorType is mandatory
            if _element_name == "contributor":
                self.add_attribute(_person, "contributorType", _person_dict, _current_dir)

            # 'nameIdentifierScheme' is mandatory if 'nameIdentifier' exists
            _required_list = ["nameIdentifier", "nameIdentifierScheme"]
            if all(key in _person_dict.get_content() for key in _required_list):
                name_id = self.add_element(_person, "nameIdentifier", _person_dict, _current_dir)
                self.add_attribute(name_id, "nameIdentifierScheme", _person_dict, _current_dir)

            self.add_element(_person, "affiliation", _person_dict, _current_dir)
            # 'affiliationIdentifierScheme' is mandatory if 'affiliationIdentifier' exists
            _required_list = ["affiliationIdentifier", "affiliationIdentifierScheme"]
            if all(key in _person_dict.get_content() for key in _required_list):
                affiliation_id = self.add_element(
                    _person, "affiliationIdentifier", _person_dict, _current_dir
                )
                self.add_attribute(
                    affiliation_id, "affiliationIdentifierScheme", _person_dict, _current_dir
                )

        pass

    def add_citation(self, _sub_element, _citation_dict, _current_dir):
        """
        Adds information of a person to an ET object according to the dataCite citation schema
        (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _sub_element: Python handle to the Etree sub-element where the citation should be added.
        :param _citation_dict: Dictionary containing key-value pairs providing information about the citation to be
        added.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        # according to dataCite schema
        self.add_element(_sub_element, "title", _citation_dict, _current_dir)
        self.add_element(_sub_element, "citation", _citation_dict, _current_dir)
        self.add_element(_sub_element, "DOI", _citation_dict, _current_dir)

        pass

    def add_file_list(self, _dict, _current_dir):
        """
        Adds list of raw and converted files to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "datasets"
        whose value is a list of sub-dictionaries which provide information about each file.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "datasets" in _dict.get_content():
            # list all converted datasets
            data = ET.SubElement(self.xml_root, "datasets")
            for _sub_dict in _dict.get_value("datasets", _current_dir):
                data_dict = ConfigDict()
                data_dict.read_dict(_sub_dict)
                self.add_file(data, data_dict, _current_dir)

        pass

    def add_citation_list(self, project_dict, _current_dir):
        """
        Adds list of citations to the README page.
        :param project_dict: Dictionary containing the key "citations" whose value is a list of sub-dictionaries which
        provide information about each citation.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "citations" in project_dict.get_content():
            citations = ET.SubElement(self.xml_root, "citations")
            for _key, _sub_dict in project_dict.get_value("citations").items():
                cite_dict = ConfigDict()
                cite_dict.read_dict(_sub_dict)
                cite = ET.SubElement(citations, "citation")
                self.add_citation(cite, cite_dict, _current_dir)

        pass

    def add_creator_list(self, _dict, _current_dir):
        """
        Adds list of creators to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "creators"
        whose value is a list of sub-dictionaries which provide information about each creator.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "creators" in _dict.get_content():
            _creators = ET.SubElement(self.xml_root, "creators")
            for _key, _sub_dict in _dict.get_value("creators", _current_dir).items():
                _person_dict = ConfigDict()
                _person_dict.read_dict(_sub_dict)
                self.add_person(_creators, "creator", _person_dict, _current_dir)

        pass

    def add_contributor_list(self, _dict, _current_dir):
        """
        Adds list of contributors to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key
        "contributors" whose value is a list of sub-dictionaries which provide information about each contributor.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "contributors" in _dict.get_content():
            _contributors = ET.SubElement(self.xml_root, "contributors")
            for _key, _sub_dict in _dict.get_value("contributors", _current_dir).items():
                person_dict = ConfigDict()
                person_dict.read_dict(_sub_dict)
                self.add_person(_contributors, "contributor", person_dict, _current_dir)

        pass

    def add_abstract(self, _dict, _current_dir):
        """
        Adds an abstract (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "abstract" whose
        value is a string.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "abstract" in _dict.get_content():
            _abstract_top = ET.SubElement(self.xml_root, "descriptions")
            _abstract = self.add_element(_abstract_top, "abstract", _dict, _current_dir, "description")
            self.add_xml_lang_attribute(_abstract, "en-US")
            _abstract.set("descriptionType", "Abstract")

        pass

    def add_xml_lang_attribute(self, _element, _language):
        """
        Adds an language attribute using the xml namespace
        :param _element:
        :param _language:
        :return:
        """
        _element.set(ET.QName(self.xml_ns, "lang"), _language)
        pass

    def add_subjects(self, _dict, _current_dir):
        """
        Adds a subject according to a classification scheme.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "subjects" whose
        value is a dictionary containing one or more subject dictionaries.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "subjects" in _dict.get_content():
            _subjects_dict = ConfigDict()
            _subjects_dict.read_dict(_dict.get_value("subjects"))
            _subjects = ET.SubElement(self.xml_root, "subjects")
            for _dict_name in _subjects_dict.get_keys():
                _subject_dict = ConfigDict()
                _subject_dict.read_dict(_subjects_dict.get_value(_dict_name))
                if "subject" in _subject_dict.get_keys():
                    _single_subject = self.add_element(_subjects, "subject", _subject_dict, _current_dir, "subject")
                    if "languageCode" in _subject_dict.get_keys():
                        self.add_xml_lang_attribute(_single_subject, _subject_dict.get_value("languageCode"))
                    # adds subject information using their dicitionary key names (should conform to dataCite schema)
                    subject_items = ["schemeURI", "classificationCode", "subjectScheme", "schemeURI"]
                    for _item in subject_items:
                        if _item in _subject_dict.get_keys():
                            _single_subject.set(_item, _subject_dict.get_value(_item))
        pass

    def add_title(self, _dict, _current_dir):
        """
        Adds a title (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "title" whose
        value is a string.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "title" in _dict.get_content():
            title = ET.SubElement(self.xml_root, "titles")
            sub_title = self.add_element(title, "title", _dict, _current_dir)
            self.add_xml_lang_attribute(sub_title, "en-US")

        pass

    def add_publisher(self, _dict, _current_dir):
        """
        Adds a publisher (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "publisher" whose
        value is a string.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "publisher" in _dict.get_content():
            self.add_element(self.xml_root, "publisher", _dict, _current_dir)

        pass

    def add_publication_year(self, _dict, _current_dir):
        """
        Adds the current year as publication year (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        _dict.add_value("publicationYear", str(datetime.datetime.now().year))
        self.add_element(self.xml_root, "publicationYear", _dict, _current_dir)

        pass

    def add_resource_type(self, _dict, _current_dir):
        """
        Adds the resource type according to the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if self.string_found("resourceTypeGeneral", _dict):
            self.add_element(self.xml_root, "resourceTypeGeneral", _dict, _current_dir)
        if self.string_found("resourceType", _dict):
            self.add_element(self.xml_root, "resourceType", _dict, _current_dir)

        pass

    def add_date(self, _dict, _current_dir):
        """
        Adds the creation date of the dataset (not of the measurement/data taking) according to the DataCite schema to
        the README page dictionary.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        _dates_list = ET.SubElement(self.xml_root, "dates")
        # add creation date of this dataset (i.e. this collection) as "Updated"
        dummy_dict = ConfigDict()
        dummy_dict.add_value("date", self.get_utc(datetime.datetime.now()))
        dummy_dict.add_value("dateType", "Updated")
        _date = self.add_element(_dates_list, "date", dummy_dict, _current_dir)
        self.add_attribute(_date, "dateType", dummy_dict, _current_dir)
        # add creation date(s) of data/measurement/data taking as "Created"
        if "creation_date" in _dict.get_content():
            _date = self.add_element(_dates_list, "creation_date", _dict, _current_dir, "date")
            dummy_dict.add_value("dateType", "Created")
            self.add_attribute(_date, "dateType", dummy_dict, _current_dir)
        del dummy_dict

        pass

    @staticmethod
    def get_utc(dt):
        """
        Converts a local datetime object into a string representing the UTC time with corresponding timezone shift;
        includes summer and winter time shift.
        :param dt: Datetime object representing a local time.
        :return: String representing UTC time.
        """
        timezone = pytz.timezone("Europe/Berlin")
        timezone_date = timezone.localize(dt, is_dst=None)
        part = str(timezone_date).split("+")
        utc_shift = part[1]
        time = timezone_date.strftime("%Y-%m-%dT%H:%M:%S") + "+" + utc_shift
        return str(time)

    def add_rights(self, _dict, _current_dir):
        """
        Adds the creation date according to the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if self.string_found("rights", _dict):
            _rights_list = ET.SubElement(self.xml_root, "rightsList")
            _rights = self.add_element(_rights_list, "rights", _dict, _current_dir)
            if self.string_found("rights_url", _dict):
                self.add_renamed_attribute(_rights, "rights_url", _dict, "href", _current_dir)

        pass

    def add_instrument(self, _sub_element, _item_dict, _current_dir):
        """
        Adds information of an instrument to an ET object according to the dataCite citation schema
        (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _sub_element: Python handle to the Etree sub-element where the citation should be added.
        :param _item_dict: Dictionary containing key-value pairs providing information to be added.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        _sub_element.set("relatedItemTypeGeneral", "Other")
        _sub_element.set("relatedItemType", "Instrument persistent identifier")
        _sub_element.set("relationType", "IsCompiledBy")
        if "instrument_PID" in _item_dict.get_keys():
            related_item_identifier = ET.SubElement(_sub_element, "relatedItemIdentifier")
            if _item_dict.get_value("instrument_PID").startswith("https://doi.org/"):
                _instrument_PID = _item_dict.get_value("instrument_PID").replace("https://doi.org/", "")
            else:
                _instrument_PID = _item_dict.get_value("instrument_PID")

            related_item_identifier = ET.SubElement(related_item_identifier, "relatedItemIdentifier")
            related_item_identifier.text = _instrument_PID
            related_item_identifier.set("relatedItemIdentifierType", "DOI")

            titles = ET.SubElement(_sub_element, "titles")
            title = ET.SubElement(titles, "title")
            title.text = str(_item_dict.get_value("instrument_name"))

        pass

    def add_related_item(self, _sub_element, _item_dict, _current_dir):
        """
        Adds information of a relatedItem to an ET object according to the dataCite citation schema
        (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _sub_element: Python handle to the Etree sub-element where the citation should be added.
        :param _item_dict: Dictionary containing key-value pairs providing information to be added.
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        # according to dataCite schema
        self.add_attribute(_sub_element, "relatedItemType", _item_dict, _current_dir)
        self.add_attribute(_sub_element, "relationType", _item_dict, _current_dir)

        if ("relatedItemIdentifierType" in _item_dict.get_keys()) and ("relatedItemIdentifier" in _item_dict.get_keys()):
            related_item_identifier = self.add_element(_sub_element, "relatedItemIdentifier", _item_dict, _current_dir)
            self.add_attribute(related_item_identifier, "relatedItemIdentifierType", _item_dict, _current_dir)

        # add titles
        title_list = _item_dict.get_value("titles")
        if len(title_list) > 0:
            titles = ET.SubElement(_sub_element, "titles")
            for _title_text in title_list:
                title = ET.SubElement(titles, "title")
                title.text = str(_title_text)

        self.add_element(_sub_element, "publicationYear", _item_dict, _current_dir)
        self.add_element(_sub_element, "volume", _item_dict, _current_dir)
        self.add_element(_sub_element, "firstPage", _item_dict, _current_dir)

        pass

    def add_related_items_list(self, _dict, _current_dir):
        """
        Adds an instrument image (if available) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        if ("relatedItems" in _dict.get_keys()) or ("instrument" in _dict.get_keys()):
            related_items = ET.SubElement(self.xml_root, "relatedItems")
            for _key, _sub_dict in _dict.get_value("relatedItems").items():
                item_dict = ConfigDict()
                item_dict.read_dict(_sub_dict)
                related_item = ET.SubElement(related_items, "relatedItem")
                self.add_related_item(related_item, item_dict, _current_dir)

            # add instrument PID as relatedItem
            if "instrument" in _dict.get_keys():
                instrument_dict = ConfigDict()
                instrument_dict.read_dict(_dict.get_value("instrument"))
                related_item = ET.SubElement(related_items, "relatedItem")
                self.add_instrument(related_item, instrument_dict, _current_dir)

        pass

    def add_footnotes(self, subdata_dict, current_dir):
        """
        Adds footnotes to the (run-level) readme page.
        :param subdata_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (sub-level) data sets.
        :param current_dir: String containing the path to the current directory.
        :return: None.
        """
        if "footnotes" in subdata_dict.get_keys():
            footnotes_dict = ConfigDict()
            footnotes_dict.read_dict(subdata_dict.get_value("footnotes"))
            footnotes = ET.SubElement(self.xml_root, "footnotes")

            for _key, _dict in footnotes_dict.get_items():
                _footnote = ET.SubElement(footnotes, "footnote")
                _footnote.text = _key
                if "description" in _dict:
                    _footnote.set("description", _dict["description"])
                if "link" in _dict:
                    file_name = _dict["link"].replace(" ", "_") + ".html"
                    if "output_path" in _dict:
                        _link = os.path.join(assemble_path("", _dict["output_path"]), file_name)
                    else:
                        _link = file_name
                    _footnote.set("href", _link)
                self.create_footnote_page(_link, _dict, current_dir)

        pass

    def create_footnote_page(self, html_path, _dict, _current_dir):
        """
        Creates a separate footnote html page with detailed information, e.g. about the root file format.
        :param html_path: String containing the (relative) path to the page (including the page name),
        e.g. '_supplement/file_formats/root_file/Root_Files_with_TMedusa_Class'.
        :param _dict: Dictionary containing information about the footnote (sub-element of
        config_project/subdata_meta.json).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        # remove (first) leading '..' to be at the top readme file level
        if os.path.sep in html_path:
            if html_path.split(os.path.sep, 1)[0] == "..":
                html_path = html_path.split(os.path.sep, 1)[1]

        # create absolute path
        project_path = assemble_path("", self.project_dict.get_value("output_path"))
        full_path = os.path.join(project_path, html_path)

        # only create page if it doesn't exist already
        if not os.path.isfile(full_path):
            # dummy xml file
            dummy_root = ET.Element("root", encoding="utf-8")

            # assume the last element is the file name
            if os.path.sep in html_path:
                folder_path, html_name = full_path.rsplit(os.path.sep, 1)
            else:
                folder_path = ""
                html_name = full_path

            # create path folder(s)
            create_path(folder_path)

            # copy page dict content to xml
            page_dict = {}
            if "page" in _dict:
                page_dict = _dict["page"]
                for key, value in page_dict.items():
                    dummy_key = ET.SubElement(dummy_root, key)
                    if isinstance(value, dict):
                        for sub_key, sub_value in value.items():
                            dummy_sub_key = ET.SubElement(dummy_key, key)
                            dummy_sub_key.text = sub_key
                            dummy_sub_key.set("description", sub_value)
                    elif isinstance(value, list):
                        counter = 0
                        for list_item in value:
                            counter += 1
                            if isinstance(list_item, dict):

                                dummy_sub_key = ET.SubElement(dummy_key, key)
                                dummy_sub_key.text = list_item
                            else:
                                dummy_sub_key = ET.SubElement(dummy_key, key)
                                dummy_sub_key.text = list_item
                    else:
                        dummy_key.text = value

            # copy file(s)
            if "files" in _dict:
                file_dict = _dict["files"]
                dummy_files = ET.SubElement(dummy_root, "files")
                input_path = assemble_path("", _dict["input_path"])
                for file_name, description in file_dict.items():
                    source_path = os.path.join(input_path, file_name)
                    target_path = os.path.join(folder_path, file_name)
                    shutil.copy2(source_path, target_path)

                    _file = ET.SubElement(dummy_files, "file")
                    _file.text = file_name
                    _file.set("href", file_name)
                    _file.set("description", description)

            # write ET to xml file:
            xml_name = html_name.replace(".html", ".xml")
            xml_path = os.path.join(folder_path, xml_name)
            ET.ElementTree(dummy_root).write(
                xml_path, encoding="utf-8", xml_declaration=True
                )

            # create html file
            if "template_xslt" in page_dict:
                xsl_path = assemble_path("", ["config_project", "file_formats", page_dict["template_xslt"]])
                self.xml2html(xml_path, xsl_path)

            # delete dummy xml file
            if os.path.isfile(xml_path):
                os.remove(xml_path)

        pass

    def add_datacite_core(self, _dict, _current_dir):
        """
        Adds mandatory core elements (if available) of the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :param _current_dir: String containing the path to the current directory.
        :return: None.
        """
        # announce DataCite schema to be used
        # self.add_attribute(self.xml_root, "xsi:schemaLocation", _dict, _current_dir)
        _dict.add_dict(self.project_dict, "xsi:schemaLocation")

        # add common elements if they exist
        self.add_title(_dict, _current_dir)
        self.add_creator_list(_dict, _current_dir)
        self.add_contributor_list(_dict, _current_dir)
        self.add_publisher(_dict, _current_dir)
        self.add_publication_year(_dict, _current_dir)
        self.add_resource_type(_dict, _current_dir)
        self.add_abstract(_dict, _current_dir)
        self.add_date(_dict, _current_dir)
        self.add_rights(_dict, _current_dir)
        self.add_subjects(_dict, _current_dir)
        self.add_related_items_list(_dict, _current_dir)

        pass

    def create_sub_readme(self, readme_dict, current_dir):
        """
        Writes a low-level readme file that summarizes the content of an individual dataset (e.g. the data corresponding
        to a single measurement).
        :param readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the content that has to be included in the (low-level) README_[dir_name].xml file.
        :param current_dir: String containing the path to the current directory.
        :return: None.
        """
        # read sub-data information
        subdata_dict = ConfigDict()
        subdata_dict.read_json(subdata_meta_path())

        # add subdata_meta.json and parts of project_meta.json to readme_dict
        readme_dict.add_dict(subdata_dict)
        readme_dict.add_dict(self.project_dict, "creators")
        readme_dict.add_dict(self.project_dict, "xmlns:xsi")

        # add DataCite's core elements which are considered to be mandatory
        self.add_datacite_core(readme_dict, current_dir)

        # list all converted ASCII/xml files
        self.add_file_list(readme_dict, current_dir)

        # add footnotes
        self.add_footnotes(subdata_dict, current_dir)

        pass

    def create_top_readme(self, readme_dict, current_dir):
        """
        Writes the top-level readme file that summarizes the content of all datasets, e.g. not the data of single
        measurement; for the latter see create_sub_readme().
        :param readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the content that has to be included in the (top-level) README.xml file.
        :param current_dir: String containing the path to the current directory.
        :return: None.
        """
        # add DataCite's core elements which are considered to be mandatory
        readme_dict.add_dict(self.project_dict)
        self.add_datacite_core(readme_dict, current_dir)

        # list converted datasets
        self.add_file_list(readme_dict, current_dir)

        pass

    def create_report(self, report_dict):
        """
        Creates the report Etree object that summarizes the messages created during the data treatment.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the created messages.
        :return: None.
        """
        self.report_missing_def(report_dict)
        self.report_merged_lines(report_dict)

        pass

    def report_missing_def(self, report_dict):
        """
        Creates list of missing rdf definitions in each file that was converted (e.g. from ASCII to xml).
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the created messages.
        :return: None.
        """
        def_element = ET.SubElement(self.xml_root, "missingDefinitions")

        adding_list = []

        for _file_id in report_dict.get_keys():
            _file_dict = report_dict.get_value(_file_id)
            if "missing_rdf" in _file_dict:
                definitions_dict = _file_dict["missing_rdf"]
                for missing_dict in definitions_dict:
                    _entry = missing_dict["entry"]
                    _path = missing_dict["path"]
                    if _entry not in adding_list:
                        # find missing resource in other files
                        usage_path_list = self.get_def_usage_list(_entry, _path, _file_id, "missing_rdf", report_dict)

                        # remember that _entry was already added
                        adding_list.append(_entry)

                        # add ET object element
                        _def = ET.SubElement(def_element, "missingDefinition")
                        _def_resource = ET.SubElement(_def, "resource")
                        _def_resource.text = str(missing_dict["entry"])
                        self.add_xml_lang_attribute(_def_resource, "en-US")

                        _message = "Missing rdf definition" + self.get_file_usage_examples(usage_path_list)
                        _def_message = ET.SubElement(_def, "message")
                        _def_message.text = _message
                        self.add_xml_lang_attribute(_def_message, "en-US")

        pass

    def get_def_usage_list(self, first_entry, first_path, first_file_id, category, report_dict):
        """
        Searches for other appearances of 'first_entry' in the report dictionary and creates a list of files which share
        the 'first_entry'.
        :param first_entry: String containing the entry that was already found, e.g. "count".
        :param first_path: String containing the path that was already found,
        e.g. "D:\\a4-metadata\\input_exampleData\\A4_data_set\\run78850\\medusa\\daten".
        :param first_file_id: String containing the file_id that was already found, e.g. "daten".
        :param category: String containing the file_id that was already found, e.g. "missing_rdf".
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the created messages.
        :return: List of strings (containing absolute paths of files found).
        """
        # include the file that was already found
        usage_path_list = [first_path]

        for _file_key in report_dict.get_keys():
            # exclude the file that was already found
            if not _file_key == first_file_id:
                _file_dict = report_dict.get_value(_file_key)
                if category in _file_dict:
                    definitions_dict = _file_dict[category]
                    for missing_dict in definitions_dict:
                        _entry = missing_dict["entry"]
                        _path = missing_dict["path"]
                        if _entry == first_entry:
                            _filename = self.get_last_path_element(_path)

                            file_found = False
                            for _found_path in usage_path_list:
                                _found_filename = self.get_last_path_element(_found_path)
                                if _filename == _found_filename:
                                    file_found = True
                            if not file_found:
                                usage_path_list.append(_path)

        return usage_path_list

    @staticmethod
    def get_last_path_element(_path):
        """
        Returns the last element of a path, i.e. name of folder or file.
        :param _path: String containing a path to folder or file.
        :return: String (containing the last element of the path).
        """
        if os.path.sep in _path:
            _filename = _path.rsplit(os.path.sep, 1)[1]
        else:
            _filename = _path

        return _filename

    @staticmethod
    def get_file_usage_examples(usage_path_list):
        """
        Creates a short text string of a list by including maximum 3 comma separated items.
        :param usage_path_list: List of strings containing absolute path to files (which share a property, e.g. missing
        rdf definition of a unit).
        :return: String (containing a short version of provided list).
        """
        remaining = 0
        if len(usage_path_list) > 1:
            # restrict to maximum of 3 items
            add_etc = False
            if len(usage_path_list) > 3:
                add_etc = True
                max_no = 3
                remaining = len(usage_path_list) - 3
            else:
                max_no = len(usage_path_list)

            # add only list of file names (without absolute path)
            example_string = " in files "
            for i in range(0, max_no):
                if i > 0:
                    example_string += ", "
                example_string += usage_path_list[i].rsplit(os.path.sep, 1)[1]

            # add punctuation mark
            if add_etc:
                example_string += " (and " + str(remaining) + " other)."
            else:
                if not example_string.endswith("."):
                    example_string += "."
        elif len(usage_path_list) == 1:
            example_string = " in file " + usage_path_list[0]
            if not example_string.endswith("."):
                example_string += "."
        else:
            example_string = ""

        return example_string

    def report_merged_lines(self, report_dict):
        """
        Creates list of merged lines in each file (e.g. due to additional linebreaks) that was converted
        (e.g. from ASCII to xml).
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the created messages.
        :return: None.
        """
        def_element = ET.SubElement(self.xml_root, "mergedLines")

        for _file_id in report_dict.get_keys():
            _file_dict = report_dict.get_value(_file_id)
            if "merged_lines" in _file_dict:
                definitions_dict = _file_dict["merged_lines"]

                if len(definitions_dict) > 0:
                    merged_dict = definitions_dict[0]
                    if "path" in merged_dict:
                        _path = merged_dict["path"]
                        _def = ET.SubElement(def_element, "mergedLine")
                        _def_file = ET.SubElement(_def, "file")
                        _def_file.text = str(_path)

                        usage_in_file = self.get_usage_in_file(_file_id, "merged_lines", report_dict)
                        _message = "Merging " + self.get_line_usage_examples(usage_in_file) + " with preceding line."
                        _def_message = ET.SubElement(_def, "message")
                        _def_message.text = _message
                        self.add_xml_lang_attribute(_def_message, "en-US")
        pass

    @staticmethod
    def get_usage_in_file(file_id, category, report_dict):
        """
        Searches for other appearances of an entry within a file and creates a list of these entries, i.e. a list
        of the same manipulations that were done to the same file (to summarize it); lists the line numbers of merged
        lines in the same file.
        :param file_id: String containing the path that was already found,
        e.g. "D:\\a4-metadata\\input_exampleData\\A4_data_set\\run78850\\medusa\\daten".
        :param file_id: String containing the file_id that was already found, e.g. "daten".
        :param category: String containing the file_id that was already found, e.g. "missing_rdf".
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        defining the created messages.
        :return: List of strings (containing absolute paths of files found).
        """
        # include the file that was already found
        entry_list = []

        if file_id in report_dict.get_keys():
            _file_dict = report_dict.get_value(file_id)
            if category in _file_dict:
                definitions_dict = _file_dict[category]
                for merged_dict in definitions_dict:
                    _entry = merged_dict["entry"]
                    if _entry not in entry_list:
                        entry_list.append(_entry)

        return entry_list

    @staticmethod
    def get_line_usage_examples(usage_line_list):
        """
        Creates a short text string of a list by including maximum 3 comma separated items.
        :param usage_line_list: List of strings containing line numbers (which share a property, e.g. that they were
        merged with the preceding one).
        :return: String (containing a short version of provided list).
        """
        remaining = 0
        list_length = len(usage_line_list)
        if list_length > 1:
            # restrict to maximum of 3 items
            add_etc = False
            if list_length > 3:
                add_etc = True
                max_no = 3
                remaining = list_length - 3
            else:
                max_no = list_length

            # add only list of file names (without absolute path)
            example_string = " lines "
            for i in range(0, max_no):
                if i > 0:
                    example_string += ", "
                example_string += str(usage_line_list[i])

            # add punctuation mark
            if add_etc:
                example_string += " (and " + str(remaining) + " others)"

        elif len(usage_line_list) == 1:
            example_string = " line " + str(usage_line_list[0])
        else:
            example_string = ""

        return example_string

    @staticmethod
    def string_found(_key, _dict):
        """
        Checks if a key exists in a dictionary and is assigned to a non-empty string value.
        :param _key: String containing the key that is checked.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs.
        :return: Boolean which is True if the key exists and is a non-empty string.
        """
        was_found = False
        if _key in _dict.get_content():
            _value = _dict.get_value(_key)
            if isinstance(_value, str) and not _value == "":
                was_found = True

        return was_found
