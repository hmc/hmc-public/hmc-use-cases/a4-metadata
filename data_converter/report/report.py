# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import json
import os
from pathlib import Path
import sys

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.config_dict import *
from auxiliary.general_aux import *
from xml_writer.xml_writer import *


class ReportLog(object):
    """
    Collects report information of data handling (identified gaps, missing files, failures...).
    """

    def __init__(self):
        """
        Initializes basic elements of the report log.
        """
        self.report_dict = ConfigDict()
        pass

    def add_text_entry(self, _entry, _path, _file_id):
        """
        Adds a text message (_entry) and a path string (_path) to a list that is assigned to [_file_id][_category]
        level of the report dictionary.
        :param _entry: String containing the message that is to be written to the project log file.
        :param _path: String containing the path to the folder of the file that the current entry belongs to.
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :return: None.
        """
        _category = "general"
        self.report_dict.add_to_report(_file_id, _category, _entry, _path)
        pass

    def add_missing_file(self, _entry, _path, _file_id):
        """
        Adds a text message (_entry) and a path string (_path) to a list that is assigned to [_file_id][_category]
        level of the report dictionary. Here, the text message should contain the full filename,
        e.g. 'counts_elastic.78850'.
        :param _entry: String containing the message that is to be written to the project log file.
        :param _path: String containing the path to the folder of the file that the current entry belongs to.
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :return: None.
        """
        _category = "missing_files"
        self.report_dict.add_to_report(_file_id, _category, _entry, _path)
        pass

    def add_missing_resource(self, _entry_name, _entry, _path, _file_id):
        """
        Adds a text message (_entry) or unit and a path string (_path) to a list that is assigned to
        [_file_id][_category] level of the report dictionary. In this case, the text message should contain a header or
        unit that misses a rdf annotation, e.g. 'arb. u.' or 'count'.
        :param _entry_name: String containing the name/key of the dictionary that _entry was associated with, e.g. "name",
        "units", or "comment".
        :param _entry: String containing the message that is to be written to the project log file.
        :param _path: String containing the path to the folder of the file that the current entry belongs to.
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :return: None.
        """
        _category = "missing_rdf"
        # add only missing resources for (header) names and units but not e.g. for comments
        white_list = ["name", "units"]
        if _entry_name in white_list:
            self.report_dict.add_to_report(_file_id, _category, _entry, _path)
        pass

    def add_merged_line(self, _entry, _path, _filename):
        """
        Adds a text message (_entry) or unit and a path string (_path) to a list that is assigned to
        [_file_id][_category] level of the report dictionary. In this case, the text message should contain the line
        number that was merged with the preceding line, e.g. "479".
        :param _entry: String containing the message that is to be written to the project log file.
        :param _path: String containing the path to the folder of the file that the current entry belongs to.
        :param _filename: String containing the name of the file, e.g. 'info.78850'.
        :return: None.
        """
        _category = "merged_lines"
        self.report_dict.add_to_report(_filename, _category, _entry, _path)

        pass

    @staticmethod
    def get_project_report_path():
        """
        Reads path and filename of the project report file from file 'config/project_meta.json'.
        :return: String (containing the path of the project report file).
        """
        project_dict = ConfigDict()
        project_dict.read_json(project_meta_path())
        path_found, log_path = project_dict.confirm_assemble_path("report_file_path")
        if path_found:
            # otherwise, use the data's output_path to create the report log file (same level as the created
            # dataset folder, e.g. D:/A4-metadata/output/)
            log_path = project_dict.assemble_path("report_file_path")

        if not path_found:
            path_found, log_path = project_dict.confirm_assemble_path("output_path")

        if not path_found:
            log_path = os.path.join(".", "report.xml")

        return log_path

    def create_report(self, top_readme_dict):
        """
        Creates xml and html files from the report dictionary-like object.
        :param top_readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the summary of
        the data set to be written to the readme file at the top level of the dataset.
        :return: None.
        """
        # generate path and filename of report
        report_file_path = self.get_project_report_path()
        report_path, report_file = report_file_path.rsplit(os.path.sep, 1)
        create_path(report_path)
        if not report_file.endswith(".xml"):
            if "." in report_file:
                report_file = report_file.rsplit(".", 1)[0] + ".xml"
            else:
                report_file = report_file + ".xml"

        # create report xml file
        report = XMLWriter('resource', report_file)
        report.create_header()
        report.create_report(self.report_dict)
        report.write_xml(report_file, report_path)

        # create report html file
        style_sheet_path = os.path.join(
            config_project_path(), "report.xslt"
        )
        report_file_html = report.xml2html(
            os.path.join(report_path, report_file), style_sheet_path
        )

        # get report's relative path to data output path
        project_dict = ConfigDict()
        project_dict.read_json(project_meta_path())
        output_path = project_dict.assemble_path("output_path")
        rel_report_path = os.path.relpath(report_path, output_path)

        # add report html log file to top-level readme
        dataset_dict = ConfigDict()
        dataset_dict.add_value("output_path", rel_report_path.split(os.path.sep))
        _description = "Report log file of data converter."
        dataset_dict.add_value("description", _description)
        top_readme_dict.add_file(report_file_html, dataset_dict, "", False)

        pass
