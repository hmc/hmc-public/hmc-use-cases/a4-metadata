# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import datetime
import numpy as np
import os
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))


from converter.ascii_converter import *


class ConvertHallSummary(object):
    """
    Converts ASCII files of the A4 experiment with name hall_summary.dat to xml format. There is a mix of dates, floats,
    and strings in the tabel and that is why this file is handled by this custom class.
    """

    def __init__(self, _current_dir, _readme_dict, _dict):
        """
        Initializes class variables.
        :param _current_dir: String containing the path to the current folder.
        :param _readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (lower-level) readme file that should be written for each data set (which this file is part of).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing various information as
        delivered by _sum_dict in the routine process_file() of file_handler.py such as file_dict, config_dict,
        output_path...
        :return: None.
        """
        self.current_dir = _current_dir
        self.readme_dict = _readme_dict

        self.config_dict = ConfigDict()
        if "config_dict" in _dict.get_keys():
            self.config_dict.read_dict(_dict.get_value("config_dict"))

        self.file_dict = ConfigDict()
        if "file_dict" in _dict.get_keys():
            self.file_dict.read_dict(_dict.get_value("file_dict"))

        self.output_path = ""
        if "output_path" in _dict.get_keys():
            self.output_path = _dict.get_value("output_path")

        self.parent_output_path = ""
        if "parent_output_path" in _dict.get_keys():
            self.parent_output_path = _dict.get_value("parent_output_path")

        self.filename = ""
        if "filename" in _dict.get_keys():
            self.filename = _dict.get_value("filename")

        pass

    def start(self):
        """
        Routine that reads the ASCII file, coverts its content to an Etree object and writes the xml file.
        :return: None.
        """
        # generate paths
        filename = "hall_summary.dat"
        raw_file_found, raw_filename = guess_file(self.current_dir, filename)
        if raw_file_found:
            raw_file_path = os.path.join(self.current_dir, raw_filename)

            with open(raw_file_path) as file:
                line_list = file.readlines()

            # write array to xml file column-wise
            xml_filename = filename.split(".")[0] + ".xml"
            Writer = XMLWriter("resource", filename)
            Writer.create_header()

            _list = []
            for line in line_list:
                _line_list = []
                # add combined first two columns as UTC time
                _line_list.append(self.create_utc(line.split()[0], line.split()[1]))
                # add remaining columns unchanged
                for i in range(2, 6):
                    _line_list.append(line.split()[i])
                _list.append(_line_list)

            array_2dim = np.array(_list).T
            Writer.array2xml(array_2dim, self.file_dict, self.current_dir, self.file_dict)
            Writer.write_xml(xml_filename, self.output_path)
            del Writer

            # add converted file to sub-readme dictionary
            self.readme_dict.add_file(xml_filename, self.file_dict, self.parent_output_path, False)

            # calling the ASCII converter class by using the generated _file_dict
            converter = ASCIIConverter()
            converter.copy_original(raw_file_path,
                                    self.output_path,
                                    self.parent_output_path,
                                    True,
                                    xml_filename,
                                    "ASCII",
                                    self.readme_dict,
                                    self.file_dict)
            del converter

        pass

    @staticmethod
    def create_utc(date_string, time_string):
        """
        :param date_string: String containing the date in format DD.MM.YYYY, e.g. '29.10.2010'.
        :param time_string: String containing the time in format hh:mm:ss, e.g. '11:38:00'.
        :return: String (containing the date and time according to UTC).
        """
        _datetime_string = date_string + "T" + time_string
        time_format = "%d.%m.%YT%H:%M:%S"
        dt = datetime.datetime.strptime(_datetime_string, time_format)
        return get_utc(dt)
