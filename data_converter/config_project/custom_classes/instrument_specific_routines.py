# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.general_aux import *


def get_a4_creation_date(subdata_path, readme_dict, report_dict):
    """
    Reads start and end date of measurement from elog_run_entry.log file and returns them as a string in ISO format.
    :param subdata_path: String containing the path to the folder containing the data set to be converted,
    e.g. 'D:/a4-metadata/input_exampleData/A4_data_set'.
    :param readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the summary of
    the data set to be written to the readme file at the sub-directory level of the dataset; will contain an entry
    for every file within the data set.
    :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
    detail the course of the automatic process.
    :return: String containing the start and end date in ISO format,
    i.e. YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD.
    """
    creation_range = ""
    file_name = "elog_run_entry.log"
    if find_file(subdata_path, file_name):
        # read file as list of strings (using 'True')
        log_file = file2string(subdata_path, file_name, True)
        # get date line
        start_raw = get_a4_log_line(log_file, "Date", ":")
        start_iso = a4_date2iso(start_raw)
        start_date = datetime.datetime.fromisoformat(start_iso)
        # get measurement duration
        measurement_seconds = int(get_a4_log_line(log_file, "Runtime", "=").split('s')[0])
        # calculate end date through [start date + measurement duration]
        end_date = start_date + datetime.timedelta(seconds=measurement_seconds)
        end_iso = end_date.isoformat()
        creation_range = start_iso + "/" + end_iso

    return creation_range


def get_a4_log_line(log_file, keyword, _sep):
    """
    Returns the remaining string following keyword and separator (_sep), assuming the line follows the structure
    'keyword-_sep-string', e.g. 'Date: Fri, 29 Oct 2010 11:38:17 +0200'.
    :param log_file: String containing the content of the elog_run_entry.log file.
    :param keyword: String containing the keyword, e.g. 'Date'.
    :param _sep: String containing the separator, e.g. ':'.
    :return: String containing the remaining part of the identified line, e.g. 'Fri, 29 Oct 2010 11:38:17 +0200'.
    """
    last_part = ""
    for line in log_file:
        if line.strip().startswith(keyword):
            last_part = line.split(_sep, 1)[1].strip()
    return last_part


def a4_date2iso(date_raw):
    """
    Returns the date given in the elog_run_entry.log file (e.g. 'Fri, 29 Oct 2010 11:38:17 +0200') on ISO format of
    the form 'YYYY-MM-DDThh:mm:ssTZD'.
    :param date_raw: String containing the date in the format of the A4 elog_run_entry.log file.
    :return: String containing the date in ISO format of the form 'YYYY-MM-DDThh:mm:ssTZD'.
    """
    date_iso = ""
    date_parts = date_raw.split(',')[1].strip().split()
    if len(date_parts) == 5:
        day, month_abbr, year, time, timezone = date_parts
        date_iso = year + "-" + month2dig(month_abbr) + "-" + day + "T" + time + timezone[:-2] + ":00"
    return date_iso