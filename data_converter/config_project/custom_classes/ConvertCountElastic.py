# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import json
import os
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from converter.ascii_converter import *


class ConvertCountElastic(object):
    """
    Converts ASCII files of the A4 experiment with name count_elastic.[run_number] to xml format. A single line of the
    ASCII file comprises 200 columns grouped in 40 blocks of 5 columns with same structure. That is why this file is
    handled by this custom class.
    """

    def __init__(self, _current_dir, _readme_dict, _dict):
        """
        Initializes class variables.
        :param _current_dir: String containing the path to the current folder.
        :param _readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (lower-level) readme file that should be written for each data set (which this file is part of).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing various information as
        delivered by _sum_dict in the routine process_file() of file_handler.py such as file_dict, config_dict,
        output_path...
        """
        self.current_dir = _current_dir
        self.readme_dict = _readme_dict

        self.config_dict = ConfigDict()
        if "config_dict" in _dict.get_keys():
            self.config_dict.read_dict(_dict.get_value("config_dict"))

        self.file_dict = ConfigDict()
        if "file_dict" in _dict.get_keys():
            self.file_dict.read_dict(_dict.get_value("file_dict"))

        self.output_path = ""
        if "output_path" in _dict.get_keys():
            self.output_path = _dict.get_value("output_path")

        self.parent_output_path = ""
        if "parent_output_path" in _dict.get_keys():
            self.parent_output_path = _dict.get_value("parent_output_path")

        self.filename = ""
        if "filename" in _dict.get_keys():
            self.filename = _dict.get_value("filename")

        pass

    def ____start(self):
        """
        Routine that reads the ASCII file, coverts its content to an Etree object and writes the xml file.
        :return: None.
        """
        # generate paths
        raw_file_found, raw_filename = guess_file(self.current_dir, "counts_elastic")
        if raw_file_found:
            raw_file_path = os.path.join(self.current_dir, raw_filename)

            # generate file_dict
            _file_dict = ConfigDict()
            _key_list = ["relative_path", "output_path", "filename_part", "description"]
            for _key in _key_list:
                _file_dict.add_value(_key, self.file_dict.get_value(_key))
            _file_dict.add_value("file_format", "ASCII")
            _file_dict.add_value("convert_to", "xml")
            _file_dict.add_value("content_type", "table")

            # create 200 column headers by performing a loop over 40 blocks each creating 5 columns
            _header_dict = {}
            _definitions_dict = {}
            _def_dict = {}

            # first column of row is channel number
            _column_dict = {}
            _column_dict["name"] = "channel number"
            _column_dict["data_type"] = "integer"
            _column_dict["units"] = "dimensionless"
            _column_dict["comment"] = "Numbering of detector unit (and, thus, related to detector position)."
            _header_dict["col 0"] = _column_dict

            _column_counter = 0
            for _block_no in range(1, 42):
                # column 1

                _column_counter += 1
                _column_dict = {}
                _block_name = "integral " + str(_block_no)
                _column_dict["name"] = _block_name
                _column_dict["data_type"] = "float"
                _column_dict["units"] = "dimensionless"
                _column_dict["comment"] = "An integral lower cut in terms of its distance from the elastic peak,\
                    measured in energy resolution sigma. This value ranges from 0 to 4.0, in steps of 0.1."
                _header_dict["col " + str(_column_counter)] = _column_dict

                # column 2
                _column_counter += 1
                _column_dict = {}
                _column_name = _block_name + ": lower ADC"
                _column_dict["name"] = _column_name
                _column_dict["data_type"] = "integer"
                _column_dict["units"] = "dimensionless"
                _column_dict["comment"] = "The integral lower cut in terms of the ADC channel number."
                _header_dict["col " + str(_column_counter)] = _column_dict

                # add definition of column 2
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                             "value": "https://doi.org/10.1351/goldbook.A00320"
                }
                _def_dict = {
                             "rel": _rel_dict,
                             "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 3
                _column_counter += 1
                _column_dict = {}
                _column_name = _block_name + ": upper ADC"
                _column_dict["name"] = _column_name
                _column_dict["data_type"] = "integer"
                _column_dict["units"] = "dimensionless"
                _column_dict["comment"] = "An integral upper cut in terms of the ADC channel number, which is fixed as\
                    the spectrum edge."
                _header_dict["col " + str(_column_counter)] = _column_dict

                # add definition of column 3
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.A00320"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 4
                _column_counter += 1
                _column_dict = {}
                _column_name = _block_name + ": events for pol 0"
                _column_dict["name"] = _column_name
                _column_dict["data_type"] = "integer"
                _column_dict["units"] = "count"
                _column_dict["comment"] = "Number of events in the integral window for polarisation 0."
                _header_dict["col " + str(_column_counter)] = _column_dict

                # add definition of column 4
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.P04712"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 5
                _column_counter += 1
                _column_dict = {}
                _column_name = _block_name + ": events for pol 1"
                _column_dict["name"] = _column_name
                _column_dict["data_type"] = "integer"
                _column_dict["units"] = "count"
                _column_dict["comment"] = "Number of events in the integral window for polarisation 1."
                _header_dict["col " + str(_column_counter)] = _column_dict

                # add definition of column 5
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.P04712"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

            _file_dict.add_value("header", _header_dict)

            # print specific definition file
            def_filename = "definition_" + self.filename.split(".")[0] + ".json"
            with open(os.path.join(config_project_path(), def_filename), 'w') as f:
                json.dump(_definitions_dict, f)

            # calling the ASCII converter class by using the generated _file_dict
            converter = ASCIIConverter()
            converter.handle_file(raw_file_path, self.parent_output_path,
                                  self.output_path, _file_dict, self.config_dict, self.readme_dict, None)
            del converter

        pass
















    def start(self):
        """
        Routine that reads the ASCII file, coverts its content to an Etree object and writes the xml file.
        :return: None.
        """
        # generate paths
        raw_file_found, raw_filename = guess_file(self.current_dir, "counts_elastic")
        if raw_file_found:
            raw_file_path = os.path.join(self.current_dir, raw_filename)

            # generate file_dict
            _file_dict = ConfigDict()
            _key_list = ["relative_path", "output_path", "filename_part", "description", "footnote"]
            for _key in _key_list:
                _file_dict.add_value(_key, self.file_dict.get_value(_key))
            _file_dict.add_value("file_format", "ASCII")
            _file_dict.add_value("convert_to", "xml")
            _file_dict.add_value("content_type", "table")

            _file_dict.add_value("expected_columns", "206")

            # create 200 column headers by performing a loop over 40 blocks each creating 5 columns
            _header_dict = {}
            _definitions_dict = {}
            _def_dict = {}

            # rearrange data in sub-tables, starting with column 2 since first column will be added to each sub-table

            for table_no in range(0, 41):
                _table_dict = {}
                _column_counter = 0
                # create sub-table
                _table_dict = {"name": "integral " + str(table_no),
                               "comment": "Integral over channel number ranging from {:.1f}".format(table_no*0.1) +
                                          " to {:.1f}".format((table_no+1)*0.1) +
                                          " related to the distance from the elastic peak"
                                          ", measured in energy resolution sigma.",
                               "header": {}}

                # column 1 is always channel number (first column of original file, "use_col": 0)
                _column_dict = {"name": "channel number",
                                "data_type": "integer",
                                "units": "dimensionless",
                                "comment": "Numbering of detector unit (and, thus, related to detector position).",
                                "use_col": 0}
                _table_dict["header"]["col 0"] = _column_dict

                # ignore next column with integral's lower boundary (don't write to xml)
                # (already implemented in table comment, see sub-table above)
                _column_counter += 1
                _column_dict = {"use_col": -1}
                _table_dict["header"]["col 1"] = _column_dict

                # column 2
                _column_counter += 1
                _column_name = "lower ADC"
                _column_dict = {"name": _column_name,
                                "data_type": "integer",
                                "units": "dimensionless",
                                "comment": "The integral lower cut in terms of the ADC channel number."}
                _table_dict["header"]["col 2"] = _column_dict

                # add definition of column 2
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.A00320"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 3
                _column_counter += 1
                _column_name = "upper ADC"
                _column_dict = {"name": _column_name,
                                "data_type": "integer",
                                "units": "dimensionless",
                                "comment": "An integral upper cut in terms of the ADC channel number,"
                                           "which is fixed as the spectrum edge."}
                _table_dict["header"]["col 3"] = _column_dict

                # add definition of column 3
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.A00320"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 4
                _column_counter += 1
                _column_name = "events for pol 0"
                _column_dict = {"name": _column_name,
                                "data_type": "integer",
                                "units": "count",
                                "comment": "Number of events in the integral window for polarisation 0."}
                _table_dict["header"]["col 4"] = _column_dict

                # add definition of column 4
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.P04712"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                # column 5
                _column_counter += 1
                _column_name = "events for pol 1"
                _column_dict = {"name": _column_name,
                                "data_type": "integer",
                                "units": "count",
                                "comment": "Number of events in the integral window for polarisation 1."}
                _table_dict["header"]["col 5"] = _column_dict

                # add definition of column 5
                _rel_dict = {"value": "seeAlso",
                             "namespace_prefix": "rdfs",
                             "namespace": "http://www.w3.org/2000/01/rdf-schema#"}
                _resource_dict = {
                    "value": "https://doi.org/10.1351/goldbook.P04712"
                }
                _def_dict = {
                    "rel": _rel_dict,
                    "resource": _resource_dict
                }
                _definitions_dict[_column_name] = _def_dict

                _table_id = "col " + str(table_no)
                _header_dict[_table_id] = _table_dict

            _file_dict.add_value("header", _header_dict)

            # print specific definition file
            def_filename = "definition_" + self.filename.split(".")[0] + ".json"
            with open(os.path.join(config_project_path(), def_filename), 'w') as f:
                json.dump(_definitions_dict, f)

            # calling the ASCII converter class by using the generated _file_dict
            converter = ASCIIConverter()
            converter.handle_file(raw_file_path, self.parent_output_path,
                                  self.output_path, _file_dict, self.config_dict, self.readme_dict, None)
            del converter

        pass
