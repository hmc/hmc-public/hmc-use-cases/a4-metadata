# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import datetime
import numpy as np
import os
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))


from converter.ascii_converter import *


class ConvertLumiLog(object):
    """
    Converts ASCII files of the A4 experiment with name hall_summary.dat to xml format. There is a mix of dates, floats,
    and strings in the tabel and that is why this file is handled by this custom class.
    """

    def __init__(self, _current_dir, _readme_dict, _dict):
        """
        Initializes class variables.
        :param _current_dir: String containing the path to the current folder.
        :param _readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (lower-level) readme file that should be written for each data set (which this file is part of).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing various information as
        delivered by _sum_dict in the routine process_file() of file_handler.py such as file_dict, config_dict,
        output_path...
        :return: None.
        """
        self.current_dir = _current_dir
        self.readme_dict = _readme_dict

        self.config_dict = ConfigDict()
        if "config_dict" in _dict.get_keys():
            self.config_dict.read_dict(_dict.get_value("config_dict"))

        self.file_dict = ConfigDict()
        if "file_dict" in _dict.get_keys():
            self.file_dict.read_dict(_dict.get_value("file_dict"))

        self.output_path = ""
        if "output_path" in _dict.get_keys():
            self.output_path = _dict.get_value("output_path")

        self.parent_output_path = ""
        if "parent_output_path" in _dict.get_keys():
            self.parent_output_path = _dict.get_value("parent_output_path")

        self.filename = ""
        if "filename" in _dict.get_keys():
            self.filename = _dict.get_value("filename")

        self.xml_dict = ConfigDict()

        pass

    def start(self):
        """
        Routine that reads the ASCII file, coverts its content to an Etree object and writes the xml file.
        :return: None.
        """
        # generate paths
        filename = self.file_dict.get_value("filename_part")
        raw_file_found, raw_filename = guess_file(self.current_dir, filename)
        if raw_file_found:
            raw_file_path = os.path.join(self.current_dir, raw_filename)

            with open(raw_file_path) as file:
                line_list = file.readlines()

            # find data blocks
            prep_block, run_block, readout_block = self.get_blocks(line_list)

            xml_filename = filename.split(".")[0] + ".xml"
            Writer = XMLWriter("resource", filename)
            Writer.create_header()
            et_data = Writer.add_root_element("data")

            run_start_found = False
            readout_start_found = False
            # convert file line by line
            for line in line_list:
                # remove leading or trailing whitespaces
                line = line.strip()
                if not line.startswith("-") and not line.startswith("="):
                    # identify line by beginning
                    if line.startswith("Runnumber"):
                        self.add_runnumber(line, et_data, Writer)
                    elif line.startswith("Runtime"):
                        self.add_simple_line("run_time", line, et_data, Writer, "s")
                    elif line.startswith("Number of gates"):
                        self.add_simple_line("number_of_gates", line, et_data, Writer)
                    elif line.startswith("Mode"):
                        self.add_simple_line("mode", line, et_data, Writer, "ms")
                    elif line.startswith("purpose"):
                        self.add_simple_line("purpose", line, et_data, Writer)
                    elif line.startswith("comment"):
                        self.add_simple_line("comment", line, et_data, Writer)
                    elif line.startswith("5V ADC Testvoltage"):
                        _prep_node = Writer.create_node(et_data, "preparation")
                        self.add_adc(line, _prep_node, Writer)
                        self.add_setup_block(prep_block, _prep_node, Writer)
                    elif line.startswith("Start run at:"):
                        _run_node = Writer.create_node(et_data, "run")
                        self.add_simple_date("run_start", line, _run_node, Writer)
                        run_start_found = True
                    elif line.startswith("Stop run at:") and run_start_found:
                        self.add_simple_date("run_stop", line, _run_node, Writer)
                        self.add_setup_block(run_block, _run_node, Writer)

                    elif line.startswith("Start readout at:"):
                        _readout_node = Writer.create_node(et_data, "readout")
                        self.add_simple_date("readout_start", line, _readout_node, Writer)
                        readout_start_found = True
                    elif line.startswith("Stop readout at:") and readout_start_found:
                        self.add_simple_date("readout_stop", line, _readout_node, Writer)
                        self.add_readout_block(readout_block, _readout_node, Writer)

            Writer.write_xml(xml_filename, self.output_path)
            del Writer

            # add converted file to sub-readme dictionary
            self.readme_dict.add_file(xml_filename, self.file_dict, self.parent_output_path, False)

            # calling the ASCII converter class to copy original file and to add it to the readme
            converter = ASCIIConverter()
            converter.copy_original(raw_file_path,
                                    self.output_path,
                                    self.parent_output_path,
                                    True, xml_filename,
                                    "ASCII",
                                    self.readme_dict,
                                    self.file_dict)
            del converter

        pass

    @staticmethod
    def get_blocks(line_list):
        """
        Identifies the data blocks within the lines and returns them part-wise.
        :param line_list: List of strings containing the lumi log file.
        :return: Tuple of lists (containing the lines of the corresponding block).
        """
        counter_equals = 0
        prep_block = []
        run_block = []
        readout_block = []
        for line in line_list:
            # remove leading or trailing whitespaces
            line = line.strip()
            if line.startswith("="):
                counter_equals += 1
            else:
                if counter_equals == 3:
                    prep_block.append(line)
                elif counter_equals == 5:
                    run_block.append(line)
                elif counter_equals == 7:
                    readout_block.append(line)

        return prep_block, run_block, readout_block

    def add_runnumber(self, _line, _et_element, Writer):
        """
        Separates components of the line starting with 'Runnumber' and adds them to the xml file.
        :param _line: String containing the content of a single line of the log file.
        :param _et_element: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :return: None.
        """
        # use only the first two ':' since remaining ones belong to the time declaration
        _parts = _line.split(":", 2)
        # run number
        _key = "run_number"
        self.xml_dict.add_value(_key, _parts[0].replace("Runnumber", "").strip())
        Writer.add_element(_et_element, _key, self.xml_dict, self.current_dir)
        _key = "preparation_start"
        self.add_date(_key, _parts[2], _et_element, Writer)

        pass

    def add_date(self, _key, _line, _et_element, Writer, element_name=""):
        """
        Converts datetime given in a string in the format '[day of the week] [month] [day] [HH:MM:SS] [YYYY]' to UTC
        date and time, e.g. Fri Oct 29 11:38:13 2010.
        :param _key: String containing the name of the xml element that should be created to add the value to the file.
        :param _line: String containing the content of a single line of the log file.
        :param _et_element: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :param element_name: String containing the name that should be used for the xml file; optional. The name of
        _key is used if none or empty string is provided.
        :return: None.
        """
        if element_name == "":
            element_name = _key
        # preparation date
        _date_parts = _line.split()
        if len(_date_parts) > 4:
            month_str = _date_parts[1]
            day_str = _date_parts[2]
            time_str = _date_parts[3]
            year_str = _date_parts[4]

            self.xml_dict.add_value(_key, self.create_utc(month_str, day_str, time_str, year_str))
            Writer.add_element(_et_element, _key, self.xml_dict, self.current_dir, element_name)
        pass

    def add_simple_line(self, _key, _line, _et_element, Writer, _units="", element_name=""):
        """
        Separates components (separated by ':') of the line and adds them to the xml file, assuming the line follows the
        structure '[some string]: [value]'.
        :param _key: String containing the name of the xml element that should be created to add the value to the file.
        :param _line: String containing the content of a single line of the log file.
        :param _et_element: Etree handle to the element of the xml file where the data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :param _units: String containing the name of the units that should be added (must be within the string provided
        by _line); units are added if not part of _line; optional.
        :param element_name: String containing the name that should be used for the xml file; optional. The name of
        _key is used if none or empty string is provided.
        :return: None.
        """
        if element_name == "":
            element_name = _key
        add_units = False
        if ":" in _line:
            # assume that the first ':' separates terms if there are more than one ':'
            _value = _line.split(":", 1)[1].strip()
            # separate units if provided
            if not _units == "":
                if _value.endswith(_units):
                    _value = _value.replace(_units, "").strip()
                add_units = True

            # add value
            self.xml_dict.add_value(_key, _value)
            if add_units:
                _et_element = Writer.create_node(_et_element, element_name)
                element_name = "value"
            Writer.add_element(_et_element, _key, self.xml_dict, self.current_dir, element_name)

            # add units if required
            if add_units:
                dummy_dict = ConfigDict()
                dummy_dict.add_dict(self.xml_dict)
                dummy_dict.add_value("units", _units)
                Writer.add_element(_et_element, "units", dummy_dict, self.current_dir)

        pass

    def add_adc(self, _line, _et_element, Writer):
        """
        Separates components of the line starting with '5V ADC Testvoltage' and adds them to the xml file.
        :param _line: String containing the content of a single line of the log file.
        :param _et_element: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :return: None.
        """
        if ":" in _line:
            _parts = _line.split(":", 1)
            status = _parts[1].lower().strip()
            _sub_parts = _parts[0].split()
            voltage_units = "V"
            if status == "off":
                voltage_value = "0"
            else:
                voltage_value = _sub_parts[0].replace(voltage_units, "")
            if len(_sub_parts) > 2:
                node_name = _sub_parts[1].lower() + "_" + _sub_parts[2].lower()

                self.xml_dict.add_value("status", status)
                self.xml_dict.add_value("value", voltage_value)
                self.xml_dict.add_value("units", "V")

                _adc_node = Writer.create_node(_et_element, node_name)
                Writer.add_element(_adc_node, "status", self.xml_dict, self.current_dir)
                Writer.add_element(_adc_node, "value", self.xml_dict, self.current_dir)
                Writer.add_element(_adc_node, "units", self.xml_dict, self.current_dir)

        pass

    def add_setup_block(self, _block, _node, Writer):
        """
        Adds the data provided by the text block of the setup for preparation and run.
        :param _block: List of strings containing the block with setup details of the preparation or run.
        :param _node: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :return: None.
        """
        for _line in _block:
            if _line.startswith("|Foerster:"):
                key_list = ["Foerster", "Current"]
                name_list = ["Foerster_probe", "current"]
                unit_list = ["Hz", "muA"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|Lambda/2:"):
                key_list = ["Lambda/2", "Target Readout Time"]
                name_list = ["lambda_half", "target_readout_time"]
                unit_list = ["", ""]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|T1:"):
                key_list = ["T1", "T2", "T3", "T4"]
                name_list = ["temperature_1", "temperature_2", "temperature_3", "temperature_4"]
                unit_list = ["K", "K", "K", "K"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|T5:"):
                key_list = ["T5", "T6", "T7", "T8"]
                name_list = ["temperature_5", "temperature_6", "temperature_7", "temperature_8"]
                unit_list = ["K", "K", "K", "K"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|H-pressure:"):
                key_list = ["H-pressure", "Vac-pressure"]
                name_list = ["H_pressure", "vacuum_pressure"]
                unit_list = ["bar", "nbar"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|Bypass in percent open:"):
                key_list = ["Bypass in percent open", "H-pump"]
                name_list = ["bypass_opening", "H_pump"]
                unit_list = ["percent", "Hz"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|H-Heater:"):
                key_list = ["H-Heater", "He-Heater"]
                name_list = ["H_heater", "He_heater"]
                unit_list = ["W", "W"]
                self.add_complex_line(key_list, name_list, _line, _node, Writer, unit_list)

        pass

    def add_readout_block(self, _block, _node, Writer):
        """
        Adds the data provided by the text block given for the readout process.
        :param _block: List of strings containing the block with details about the readout process.
        :param _node: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :return: None.
        """
        for _line in _block:
            if _line.startswith("|pimo"):
                key_list = ["mean", "rms", "asym"]
                name_list = ["mean", "rms", "asymmetry"]
                unit_list = ["", "", ""]
                self.add_readout_line(key_list, name_list, _line, _node, Writer, unit_list)
            if _line.startswith("|xymo") or _line.startswith("|enmo"):
                key_list = ["mean", "rms", "diff"]
                name_list = ["mean", "rms", "difference"]
                unit_list = ["", "", ""]
                self.add_readout_line(key_list, name_list, _line, _node, Writer, unit_list)
        pass

    def add_readout_line(self, key_list, name_list, _line, _node, Writer, unit_list):
        """
        Adds a line of the 'readout block' by using the first term (e.g. 'pimo27') as the node name for the xml file and
        adding the remaining data by calling aels.add_complex_line().
        :param key_list: List of strings containing the keys to identify different key-value pairs within a line.
        :param name_list: List of strings containing the names to be used in the xml file to add the key-value pairs.
        :param _line: String containing a single line.
        :param _node: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :param unit_list: List of strings containing the units of the values.
        :return: None.
        """
        # use the first term to create the node, e.g. following 'pimo27'...
        element_name = _line.split("|", 1)[1].split()[0]
        _sub_node = Writer.create_node(_node, element_name)
        # ...and add the following part
        _line_part = _line.split(element_name, 1)[1]
        self.add_complex_line(key_list, name_list, _line_part, _sub_node, Writer, unit_list)
        pass

    def add_complex_line(self, key_list, name_list, _line, _node, Writer, unit_list):
        """
        Separates data given in a line of the setup block according to the provided key_list and adds the to the xml
        file by calling the self.add_simple_line() routine; Note: key_list, name_list, and unit_list are assumed to
        have same length and ordering.
        :param key_list: List of strings containing the keys to identify different key-value pairs within a line.
        :param name_list: List of strings containing the names to be used in the xml file to add the key-value pairs.
        :param _line: String containing a single line.
        :param _node: Etree class handle to an element of the xml file where data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :param unit_list: List of strings containing the units of the values.
        :return: None.
        """
        parts = _line.split("|")
        for counter in range(len(key_list)):
            _key = key_list[counter]
            line_part = ""
            for _part in parts:
                if _key in _part:
                    line_part = _part.strip()
            self.add_simple_line(_key, line_part, _node, Writer, unit_list[counter], name_list[counter])

        pass

    def add_simple_date(self, _key, _line, _et_element, Writer, element_name=""):
        """
        Adds the date given in a string following an ':' character assuming the format '[some key]:[date]', e.g.
        'Start run at: Fri Oct 29 11:38:17 2010'.
        :param _key: String containing the name of the xml element that should be created to add the value to the file.
        :param _line: String containing the content of a single line of the log file.
        :param _et_element: Etree handle to the element of the xml file where the data should be added.
        :param Writer: XMLWriter class (see config_project/xml_writer.py) which contains the content of the xml file to
        be written.
        :param element_name: String containing the name that should be used for the xml file; optional. The name of
        _key is used if none or empty string is provided.
        :return: None.
        """
        if element_name == "":
            element_name = _key
        if ":" in _line:
            _date = _line.split(":", 1)[1].strip()
            self.add_date(_key, _date, _et_element, Writer, element_name)
        pass

    @staticmethod
    def create_utc(month_str, day_str, time_str, year_str):
        """
        Arranges the input values to return a date and time string according to UTC.
        :param month_str: String containing the name of the month, e.g. 'October'.
        :param day_str: String containing the number of the day, e.g. '29'.
        :param time_str: String containing the time in the format hh:mm:ss, e.g. '11:37:59'.
        :param year_str: String containing the year, e.g. '2010'.
        :return: String (containing the date and time according to UTC).
        """
        _date_str = day_str + "-" + month2dig(month_str) + "-" + year_str + "T" + time_str
        time_format = "%d-%m-%YT%H:%M:%S"
        dt = datetime.datetime.strptime(_date_str, time_format)
        return get_utc(dt)
