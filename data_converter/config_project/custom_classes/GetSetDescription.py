# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import os
import re
import sys
from pathlib import Path

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.general_aux import *


class GetSetDescription(object):
    """
    Collects data/description of a single run/measurement and returns it.
    """

    def __init__(self, _current_dir, _readme_dict={}, _sum_dict={}):
        """
        Initializes class variables.
        """
        self.current_dir = _current_dir
        self.description = ""

        pass

    def start(self):
        """
        Routine that assigns data to .
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: String (containing the description of the data set to be written to the top-level readme file).
        """

        log_path = self.upward_search(self.current_dir)

        if not log_path == "":
            log_list = file2string(log_path, "elog_run_entry.log", True)

            run_time = self.word_after("Runtime =", log_list)
            foerster = self.ending_after("Foerster=", log_list)
            type = self.line_after("Type:", log_list)

            _delimiter = ", "
            self.description = "Type: " + type + _delimiter + "run time: " + run_time + _delimiter + \
                               "Foerster: " + foerster

        return self.description

    @staticmethod
    def upward_search(_path):
        # searching for file "elog_run_entry.log" by going through folders upwards (and search all child folders)
        searching = True
        _current_path = _path
        log_path = ""
        while searching:
            if os.path.sep in _current_path:
                for root, dirs, files in os.walk(_current_path):
                    for name in files:
                        if name == "elog_run_entry.log":
                            log_path = root
                            searching = False
                            break
                    if not searching:
                        break
                # going 1 level up
                _current_path = _current_path.rsplit(os.path.sep, 1)[0]
            else:
                searching = False

        return log_path

    def ending_after(self, _keyword, _string_list):
        """
        Returns the part of a word (i.e. string confined by whitespaces) following the keyword; e.g. keyword 'Foerster='
        returns '18015' of string 'example Foerster=18015 followed by some words'.
        :param _keyword: String containing the preceding part of wanted expression within the same word,
        e.g. "Foerster=".
        :param _string_list: List of strings each containing a line; one of them contains the keyword,
        e.g. 'example Foerster=18015 followed by some words'.
        :return: String (containing the part that follows _keyword).
        """
        text_string = ""
        for _line in _string_list:
            if _keyword in _line:
                search_string = re.compile(_keyword + r"\w+")
                text_string = re.search(search_string, _line)[0].replace(_keyword, "")
                break

        return text_string

    def word_after(self, _keyword, _string_list):
        """
        Returns a single word (i.e. string confined by whitespaces) following the keyword; e.g. keyword 'Runtime ='
        returns '300s' if string is 'Runtime =  300s'.
        :param _keyword: String containing the preceding part of wanted expression within the same line,
        e.g. "Runtime =".
        :param _string_list: List of strings each containing a line; one of them contains the keyword,
        e.g. 'Runtime =  300s'.
        :return: String (containing the word that follows _keyword).
        """
        text_string = ""
        for _line in _string_list:
            if _keyword in _line:
                search_string = re.compile(_keyword + r"(\s+)\w+")
                text_string = re.search(search_string, _line)[0].split(_keyword)[1]
                break

        return text_string

    def line_after(self, _keyword, _string_list):
        """
        Returns the part of a line following the keyword; e.g. keyword 'Runpurpose:' returns 'asymmetry measurement'
        if the string is 'Runpurpose: asymmetry measurement'.
        :param _keyword: String containing the preceding part of wanted expression within the same line,
        e.g. "Runpurpose:".
        :param _string_list: List of strings each containing a line; one of them contains the keyword,
        e.g. 'Runpurpose: asymmetry measurement'.
        :return: String (containing the part that follows _keyword).
        """
        text_string = ""
        for _line in _string_list:
            if _keyword in _line:
                text_string = _line.split(_keyword)[1].strip()

        return text_string
