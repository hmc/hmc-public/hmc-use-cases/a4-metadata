<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:prov="http://www.w3.org/ns/prov#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>README</title>
        <style>
          table {
            background-color: #d0d0d0;
            border: 0;
            border-radius: 10px;
            padding: 15px;
            margin-top: 10px;
            margin-bottom: 20px;
            margin-left: 10px;
            margin-right: 10px;
            vertical-align: top;
            float: left;
            border-top: 7px solid #becd00;
          }
          .box{
            position: relative;
            display: inline-block; /* Make the width of box same as image */
          }
          .box.text{
            position: absolute;
            z-index: 999;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: 0%;
            text-align: right;
            width: 90%;
          }
          div.author_box {
            margin-top: -10px;
            margin-bottom: 25px;
            margin-left: 10px;
            margin-right: 10px;
          }
          td, th {
            text-align: left;
            vertical-align: top;
            padding: 5px 20px 0px 0px;
          }
          .nobox{
            background-color: transparent;
            border: 0;
            padding: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            vertical-align: top;
            float: left;
          }
        </style>
      </head>

      <body>
        <!-- TITLE -->
        <xsl:for-each select="*">
          <h2 style="margin-left:30px;margin-top:30px"><xsl:value-of select="title"/></h2>
        </xsl:for-each>

        <!-- SYSTEM SUMMARY -->
        <div class="author_box" style="margin-left:30px">

            <td>
              <table class="nobox">
              <xsl:for-each select="*/system_summary/system_summary">
                <tr>
                  <td><xsl:value-of select="text()"/>:</td>
                  <td><xsl:value-of select="@description"/></td>
                </tr>
              </xsl:for-each>
              </table>
            </td>

        </div>

        <table class="nobox">
          <!-- DESCRIPTION -->
          <!-- This needs to be added here:  &#x2022; &nbsp;&nbsp; Subject:  Run <br>-->
          <tr>
            <td>

              <table class="box" style="width:60%">
                <tr>
                  <td colspan="2">
                    <h3>Abstract</h3>&#xa;<xsl:value-of select="*/abstract"/>
                  </td>
                </tr>
              </table>

              <table class="box" style="width:60%">
                <tr>
                  <td colspan="2">
                    <h3>Related Files</h3>
                  </td>
                </tr>
                <tr>
                  <th>File</th>
                  <th>Description</th>
                </tr>
                <xsl:for-each select="*/files/*">
                  <tr>
                      <td><a href="{@href}"><xsl:value-of select="text()"/></a></td>
                      <td><xsl:value-of select="@description"/>&#160;<sup><xsl:value-of select="@footnote"/></sup></td>
                  </tr>
                </xsl:for-each>
              </table>

              <table class="box" style="width:60%">
                <tr>
                  <td colspan="2">
                    <h3>Description</h3>
                  </td>
                </tr>
                <xsl:for-each select="*/description/description">
                  <tr>
                    <td colspan="2"><xsl:value-of select="text()"/></td>
                  </tr>
                </xsl:for-each>
              </table>

              <!-- REFERENCES -->
              <table style="width:60%">
                <tr>
                  <td colspan="2">
                    <h3>References</h3>
                  </td>
                </tr>
                <xsl:for-each select="*/urls/urls">
                  <tr>
                    <td colspan="2">
                      -&#xa;<a href="{@description}"><xsl:value-of select="text()"/></a>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>

            </td>
          </tr>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
