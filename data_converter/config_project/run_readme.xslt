<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:prov="http://www.w3.org/ns/prov#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title>README</title>
        <style>
          table {
            background-color: #d0d0d0;
            border: 0;
            border-radius: 10px;
            padding: 15px;
            margin-top: 10px;
            margin-bottom: 20px;
            margin-left: 10px;
            margin-right: 10px;
            vertical-align: top;
            float: left;
            border-top: 7px solid #becd00;
          }
          .box{
            position: relative;
            display: inline-block; /* Make the width of box same as image */
          }
          .box.text{
            position: absolute;
            z-index: 999;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: 0%;
            text-align: right;
            width: 90%;
          }
          div.author_box {
            margin-top: -10px;
            margin-bottom: 25px;
            margin-left: 10px;
            margin-right: 10px;
          }
          td, th {
            text-align: left;
            vertical-align: top;
            padding: 5px 20px 0px 0px;
          }
          .nobox{
            background-color: transparent;
            border: 0;
            padding: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            vertical-align: top;
            float: left;
          }
        </style>
      </head>

      <body>
        <!-- TITLE -->
        <xsl:for-each select="*/titles">
          <h2 style="margin-left:30px;margin-top:30px"><xsl:value-of select="title"/></h2>
        </xsl:for-each>

        <!-- AUTHORS -->
        <div class="author_box" style="margin-left:30px;margin-bottom:10px">
          <tr>
            <td>
              <xsl:for-each select="*/creators/creator">
                  <xsl:value-of select="givenName"/>&#160;<xsl:value-of select="familyName"/> <a href="{concat('https://orcid.org/', nameIdentifier)}"><img alt="ORCID logo" src="https://info.orcid.org/wp-content/uploads/2019/11/orcid_16x16.png" width="16" height="16" /></a>
                  <xsl:if test="position() != last()">
                    <xsl:text>, </xsl:text>
                  </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </div>

        <table class="nobox" style="padding-left:27px;padding-top:0px;padding-bottom:25px;width:100%">
          <xsl:for-each select="*/dates/date">
            <xsl:choose>
              <xsl:when test="@dateType='Created'">
                <tr>
                  <td style="width:5%">Start date:</td>
                  <td style="text-align:left">
                    <xsl:value-of select="substring-before(text(),'/')"></xsl:value-of>
                  </td>
                </tr>
                <tr>
                  <td style="width:5%">End date:</td>
                  <td style="text-align:left">
                    <xsl:value-of select="substring-after(text(),'/')"></xsl:value-of>
                  </td>
                </tr>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </table>

        <table class="nobox">
          <!-- KEY PARAMETERS -->
            <td>
              <table style="margin-top:-10px">
                <tr>
                  <td colspan="2" style="width:50%"> <h3>Measurement Summary</h3></td>
                </tr>
                <tr>
                   <td colspan="2"> &#xa; <xsl:value-of select="*/descriptions/description"/></td>
                </tr>
              </table>
            </td>

          <!-- INCLUDED DATA FILES -->
          <tr>
            <td style="width:50%">
              <table class="box">
                <tr>
                  <td>
                    <h3>Included Data Files</h3>
                  </td>
                </tr>
                <tr style="padding: 5px 50px 0px 0px">
                  <td colspan="2">List of converted, metadata enriched files produced from original ASCII (.dat) files which were produced during the measurement.</td>
                </tr>
                <tr><td colspan="2" style="height:10px">&#xA;</td></tr>
                <tr>
                  <th>File</th>
                  <th>Description</th>
                </tr>
                <xsl:for-each select="*/datasets/a">
                  <tr>
                    <xsl:choose>
                      <xsl:when test="@prov:generatedBy!='A4 experiment'">
                        <td><a href="{@href}"><xsl:value-of select="text()"/></a></td>
                        <td><xsl:value-of select="@description"/>&#160;<sup><xsl:value-of select="@footnote"/></sup></td>
                      </xsl:when>
                    </xsl:choose>
                  </tr>
                </xsl:for-each>
              </table>
            </td>

            <!-- SOURCE FILES -->
            <td style="width:40%">
              <table class="box">
                <tr>
                  <td>
                    <h3>Source Files</h3>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">List of original files produced during the measurement which are the source of converted XML data files.</td>
                </tr>
                <tr><td colspan="2" style="height:10px">&#xA;</td></tr>
                <tr>
                  <th>File</th>
                  <th>Description</th>
                </tr>
                <xsl:for-each select="*/datasets/a">
                  <tr>
                    <xsl:choose>
                      <xsl:when test="@prov:generatedBy='A4 experiment'">
                        <td><a href="{@href}"><xsl:value-of select="text()"/></a></td>
                        <td><xsl:value-of select="@description"/></td>
                      </xsl:when>
                    </xsl:choose>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
        </table>

        <!-- FOOTNOTES -->
        <div class="author_box">
          <table class="nobox" style="margin-left:15px;margin-top:-10px;margin-bottom:20px;width:50%">
            <xsl:for-each select="*/footnotes/footnote">&#xa;
              <tr>
                <td>
                  <sup><xsl:value-of select="text()"/>&#160;<xsl:value-of select="@description"/>&#160;
                    <a href="{@href}"><xsl:value-of select="@href"/></a>
                  </sup>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </div>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
