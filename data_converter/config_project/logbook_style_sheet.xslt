<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>A4 Logbook</title>
        <style>
          table.entry{
            background-color: #d0d0d0;
            border: 0;
            border-radius: 10px;
            padding: 15px;
            margin-top: 10px;
            margin-bottom: 20px;
            margin-left: 10px;
            margin-right: 10px;
            vertical-align: top;
            float: left;
            border-top: 7px solid #becd00;
          }
            table.entry2 {
              background-color: #d0d0d0;
              border-top: 2px;
              border-radius: 10px;
              padding: 5px;
              margin-top: 10px;
              margin-bottom: 10px;
              margin-left: 10px;
              margin-right: 10px;
              vertical-align: top;
            }
            h3.entry_header {
              border: 0;
              padding: 5px;
              margin-top: 20px;
              margin-bottom: -10px;
              margin-left: 25px;
              margin-right: 10px;
              vertical-align: top;
            }
            td, th {
              border-top: 2px;
              margin-left: 25px;
              vertical-align: top;
            }
        </style>
      </head>

      <body>
        <!-- PART 1: title, authors... -->
        <!-- TITLE -->

        <h2 style="margin-left:30px;margin-top:30px">A4 Logbook</h2>

        <!-- ENTRIES -->
        <xsl:for-each select="root/ELOG_LIST/ENTRY/item">
          <table class="entry" style="width:80%">
            <tr>
              <td colspan="2" style="padding-bottom:20px">
                <h3 class="entry_header">Entry &#160;<xsl:value-of select="MID"/>: &#160;<xsl:value-of select="Title"/></h3>
              </td>
            </tr>
            <tr>
              <td style="text-align:right;padding-right:30px;width:20%">Date:</td>
              <td style="text-align:left"><xsl:value-of select="DATE"/></td>
            </tr>
            <tr>
              <td style="text-align:right;padding-right:20px;width:20%">Subject:</td>
              <td style="text-align:left"><xsl:value-of select="Subject"/></td>
            </tr>

            <xsl:choose>
              <xsl:when test="Runpurpose">
                <tr>
                  <xsl:choose>
                    <xsl:when test="Runpurpose != ''">
                      <td style="text-align:right;padding-right:20px;width:20%">Run Purpose:</td>
                      <td style="text-align:left"><xsl:value-of select="Runpurpose"/></td>
                    </xsl:when>
                  </xsl:choose>
                </tr>
              </xsl:when>
            </xsl:choose>

            <tr>
              <xsl:choose>
                <xsl:when test="Run_Nr != ''">
                  <td style="text-align:right;padding-right:20px;width:20%">Run No.:</td>
                  <td style="text-align:left"><xsl:value-of select="Run_Nr"/></td>
                </xsl:when>
              </xsl:choose>
            </tr>
            <tr>
              <td style="text-align:right;padding-right:20px;width:20%">Type:</td>
              <td style="text-align:left"><xsl:value-of select="Type"/></td>
            </tr>

            <xsl:choose>
              <xsl:when test="Runcomment">
                <tr>
                  <xsl:choose>
                    <xsl:when test="Runcomment != ''">
                      <td style="text-align:right;padding-right:20px;width:20%">Run Comment:</td>
                      <td style="text-align:left"><xsl:value-of select="Runcomment"/></td>
                    </xsl:when>
                  </xsl:choose>
                </tr>
              </xsl:when>
            </xsl:choose>


            <tr>
              <xsl:choose>
                <xsl:when test="TEXT != ''">
                  <td style="text-align:right;padding-right:20px;width:20%">Comment:</td>
                  <td style="text-align:left"><xsl:value-of select="TEXT"/></td>
                </xsl:when>
              </xsl:choose>
            </tr>
            <tr>
              <td colspan="2" style="text-align: center; width: 80%; padding-top: 20px; padding-bottom: 20px">
                <xsl:choose>
                  <xsl:when test="ATTACHMENT">
                    <xsl:choose>
                      <xsl:when test="ATTACHMENT = ''">
                        (attachment removed)
                      </xsl:when>
                      <xsl:otherwise>
                        <img style="width: 80%">
                          <xsl:attribute name="src">
                            <xsl:value-of select="ATTACHMENT"/>
                          </xsl:attribute>
                        </img>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                </xsl:choose>
              </td>
            </tr>
          </table>

        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
