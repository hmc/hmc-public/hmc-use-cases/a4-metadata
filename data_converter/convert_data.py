# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


from file_handler import *
from auxiliary.general_aux import *
from auxiliary.config_dict import *
from elog_curator.convert_logbook import *
from report.report import *
from config_project.custom_classes.instrument_specific_routines import *


def data_conversion():
    """
    Starts the conversion of datasets which are defined in '/config_project/include_data.json'.
    :return: None.
    """
    # initiate report log
    report_dict = ReportLog()

    # read list of input data
    working_dir = get_working_directory(__file__)
    data_config_path = os.path.join(working_dir, "config_project", "include_data.json")
    data_dict = ConfigDict()
    data_dict.read_json(data_config_path)

    project_dict = ConfigDict()
    project_dict.read_json(project_meta_path())
    output_path = project_dict.assemble_path("output_path")

    # delete output_path to create fresh data set or create output_path if it doesn't exist
    clean_path(output_path)

    # make list of data sets
    path_list = []
    for _, _dict in data_dict.get_items():
        subset = ConfigDict()
        subset.read_dict(_dict)
        parent_path = subset.assemble_path("path")
        if "run_numbers" in subset.get_content():
            prefix = subset.get_value("prefix", parent_path)
            suffix = subset.get_value("suffix", parent_path)

            # create paths of folders and add them to path_list
            for number in subset.get_value("run_numbers", parent_path):
                if "-" in number:
                    boundaries = number.split("-", 1)
                    boundaries_found = False
                    try:
                        min_boundary = int(boundaries[0])
                        max_boundary = int(boundaries[1])
                        if min_boundary < max_boundary:
                            boundaries_found = True
                    except:
                        print(
                            "Input data set "
                            + prefix
                            + "["
                            + str(number)
                            + "]"
                            + suffix
                            + " not found!"
                        )

                    if boundaries_found:
                        for i in range(min_boundary, max_boundary + 1):
                            add_dir_path(prefix, str(i), suffix, parent_path, path_list)
                else:
                    add_dir_path(prefix, number, suffix, parent_path, path_list)

        del subset

    top_readme_dict = ConfigDict()

    if "e_logbook_path" in project_dict.get_content():
        # create censored logbook xml
        eln = ELogConverter()
        eln.read_xml(project_dict.assemble_path("e_logbook_path"))
        eln_filename = eln.revise_logbook(output_path, project_dict.assemble_path("elog_attachment_path"))

        # add ELN file to top-level README dictionary
        eln_dict = ConfigDict()
        eln_dict.add_value("name", eln_filename)
        eln_dict.add_to_list("output_path", "logbook")
        eln_dict.add_value("description", project_dict.get_value("e_logbook_description"))
        top_readme_dict.add_file(eln_filename, eln_dict, False)

    # start conversion for each folder path / data set
    start_date = ""
    end_date = ""
    for path in path_list:
        # convert ascii files & create dictionary readme_dict to gather information about the files of the dataset
        run_readme_dict = ConfigDict()
        # run_description = ascii2xml(path, output_path, run_readme_dict)
        file_handler = FileHandler()
        run_description = file_handler.process_file(path, output_path, run_readme_dict, report_dict)
        sub_folder = path.rsplit(os.path.sep, 1)[1]
        run_readme_dict.add_title(sub_folder)
        # get creation date from dataset (A4-specific function)
        run_dates = get_a4_creation_date(path, run_readme_dict, report_dict)
        run_readme_dict.add_creation_date(run_dates)
        start_date, end_date = update_measurement_dates(start_date, end_date, run_dates)

        # write readme_run[#no].xml file
        xml_filename = "README_" + sub_folder + ".xml"
        run_readme = XMLWriter("dataset", "README")
        run_readme.create_header()
        run_readme.create_sub_readme(run_readme_dict, path)
        run_readme.write_xml(xml_filename, os.path.join(output_path, sub_folder))

        working_dir = get_working_directory(__file__)
        style_sheet_path = os.path.join(
            working_dir, "config_project", "run_readme.xslt"
        )
        html_filename = run_readme.xml2html(
            os.path.join(output_path, sub_folder, xml_filename), style_sheet_path
        )

        # add run to project's (top-level) readme dictionary
        dataset_dict = ConfigDict()
        dataset_dict.add_value("output_path", [sub_folder])
        dataset_dict.add_value("description", run_description)
        top_readme_dict.add_file(html_filename, dataset_dict, False)

        # release memory
        del run_readme_dict
        del run_readme
        del dataset_dict

    # add total time span of measurement
    top_readme_dict.add_creation_date(start_date + "/" + end_date)

    # add supplement files to top readme file
    file_handler = FileHandler()
    file_handler.add_supplement(top_readme_dict, project_dict)

    # create report
    report_dict.create_report(top_readme_dict)
    # top_readme_dict.add_file(html_filename, dataset_dict, False)

    # create top-level readme.xml
    xml_filename = "README.xml"
    top_readme = XMLWriter('resource', "README", "1.0", True)
    top_readme.create_header()
    # doesn't pass a current directory (for execution of custom classes) since directory may change for datasets
    top_readme.create_top_readme(top_readme_dict, "")
    top_readme.write_xml(xml_filename, output_path)

    style_sheet_path = os.path.join(
            working_dir, "config_project", "project_readme.xslt"
    )
    top_readme.xml2html(
        os.path.join(output_path, xml_filename), style_sheet_path
    )

    # release memory
    del top_readme_dict
    del top_readme
    del project_dict
    del data_dict

    pass


def add_dir_path(_prefix, _number, _suffix, _parent_path, _path_list):
    """
    Creates folder path by assembling given components, checks if folder exists and, if so, adds the folder path to the
    list of paths.
    :param _prefix: String containing the first part of the folder name (before the number), e.g. "run".
    :param _number: Integer or string containing the run number, e.g. '78850'.
    :param _suffix: String containing the last part of the folder name (after the number), e.g. "".
    :param _parent_path: String containing the parent_path to the folder,
    e.g. "D:/a4-metadata/input_exampleData/A4_data_set"
    :param _path_list: List of strings each containing the path to a folder.
    :return: None. Paths are directly added to the list (pointer).
    """
    _path = os.path.join(_parent_path, _prefix + str(_number) + _suffix)
    if Path(_path).is_dir():
        _path_list.append(_path)
    else:
        print("Input data set " + _path + " not found!")

    pass


def clean_path(_path):
    """
    (Note: Not moved to auxiliary/general_aux.py to be not available to other modules.)
    Deletes output folder (defined by _output_path) if it exists to make sure that all files within the folder were
    always freshly created.
    :param _path: String containing the path to a folder were the output should be written.
    :return: None.
    """
    if Path(_path).is_dir():
        shutil.rmtree(_path)
    else:
        create_path(_path)
    pass

def update_measurement_dates(set_start, set_end, run_dates):
    """
    (Note: Not moved to auxiliary/general_aux.py to be not available to other modules.)
    Compares start and end dates of the current run with start and end dates collected from the data set and replaces
    dates to get the total time span the data set was measured.
    :param set_start: String containing the start date of the data set (consisting of various runs).
    :param set_end: String containing the end date of the data set (consisting of various runs).
    :param run_dates: String containing the date range of a run (single measurement) in the form of
    'YYYY-MM-DDThh:mm:ssTZD/YYYY-MM-DDThh:mm:ssTZD'.
    :return: Tuple of strings containing start and end date of current data set, each in form of
    'YYYY-MM-DDThh:mm:ssTZD'.
    """
    run_start, run_end = run_dates.split("/")
    # check if set dates contain dates, if not assign run dates
    if set_start == "" or set_end == "":
        set_start = run_start
        set_end = run_end

    set_start_date = datetime.datetime.fromisoformat(set_start)
    set_end_date = datetime.datetime.fromisoformat(set_end)
    run_start_date = datetime.datetime.fromisoformat(run_start)
    run_end_date = datetime.datetime.fromisoformat(run_end)

    if run_start_date < set_start_date:
        set_start = run_start
    if run_end_date > set_end_date:
        set_end = run_end

    return set_start, set_end


# standard entry point
if __name__ == "__main__":
    data_conversion()
    pass
