# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

from collections import OrderedDict
import json
import os
from pathlib import Path
from PIL import Image, EpsImagePlugin
import shutil
import sys

import pdfkit
import requests
from fpdf import FPDF
from lxml import etree
from PyPDF2 import PdfFileMerger
import xmltodict

# additionally wkhtmltopdf must be installed: https://wkhtmltopdf.org/

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.config_dict import *
from config_project.custom_classes.instrument_specific_routines import *


class ELogConverter(object):
    """
    Cleans (censors) an electronic logbook (provided as xml) for publication.
    """

    def __init__(self):
        """
        Initializes the basic components to process the eLogBook (dictionaries).
        """
        # Create constant xml header; no whitespaces allowed in _root_name
        self.origin_eln = OrderedDict()
        self.revised_eln = OrderedDict()
        self.extracted_eln = OrderedDict()

        self.blacklist = []
        self.whitelist_text = []

        self.input_path = ""

        pass

    def read_xml(self, input_path):
        """
        Reads a xml file to the dictionary.
        :return:
        """
        self.input_path = input_path
        folder_path, filename = self.input_path.rsplit(os.path.sep, 1)
        file_text = file2string(folder_path, filename)
        if not file_text == "":
            self.origin_eln = xmltodict.parse(file_text)

        pass

    def revise_logbook(self, output_path, image_path=""):
        """
        Creates a censored version of a logbook by removing content defined in self.blacklist and explicitly allowing
        content of the free-text field 'TEXT' via a whitelist (where not allowed content is removed).
        :param output_path: String containing the path to the output folder (where a separate sub-folder for the revised
        eLogBook xml file is created), e.g. 'D:/A4-metadata/output/A4_data_set'.
        :return: String containing the filename of the created xml file.
        """
        # create logbook folder in output path
        eln_folder_path = os.path.join(output_path, "logbook")
        create_path(eln_folder_path)

        # copy images
        self.copy_images(eln_folder_path, image_path)

        # read white- and blacklist content
        working_dir = get_working_directory(__file__)
        self.text_whitelist = self.json_sublist(
            os.path.join(working_dir, "..", "config_project", "logbook_whitelist.json"), "TEXT"
        )
        self.blacklist = self.json_sublist(
            os.path.join(working_dir, "..", "config_project", "logbook_blacklist.json"), "remove"
        )

        # print('starting with: ', self.origin_eln['ELOG_LIST']['ENTRY'][-1])
        self.revised_eln = OrderedDict([("ELOG_LIST", OrderedDict([("ENTRY", [])]))])
        self.extracted_eln = OrderedDict([("ELOG_LIST", OrderedDict([("ENTRY", [])]))])
        for elog_list, elog_dict in self.origin_eln.items():
            for entry, entry_list in elog_dict.items():
                for entry_dict in entry_list:
                    revised_entry_dict = OrderedDict()
                    extracted_entry_dict = OrderedDict()
                    for key, value in entry_dict.items():
                        self.revise_content(
                            key, entry_dict, revised_entry_dict, extracted_entry_dict, eln_folder_path
                        )
                    self.revised_eln["ELOG_LIST"]["ENTRY"].append(revised_entry_dict)
                    self.extracted_eln["ELOG_LIST"]["ENTRY"].append(
                        extracted_entry_dict
                    )

        eln_filename = self.input_path.rsplit(os.path.sep, 1)[1]
        output_file_path = os.path.join(eln_folder_path, eln_filename)
        self.write_xml(self.revised_eln, output_file_path)

        xslt_file_path = os.path.join(
            working_dir, "..", "config_project", "logbook_style_sheet.xslt"
        )
        html_filename = self.write_html(output_file_path, xslt_file_path)
        # self.dict2pdf(output_file_path)

        return html_filename

    def json_sublist(self, json_file_path, _key):
        """
        Reads a json file and returns a list that is assigned to a single key of the json file.
        :param json_file_path: String containing a path to a json file.
        :param _key: String containing a key of the json file whose value is a list; e.g. 'TEXT'.
        :return: List (usually of type string).
        """
        json_list = []
        json_dict = self.read_json(json_file_path)
        if _key in json_dict:
            json_list = json_dict[_key]

        return json_list

    @staticmethod
    def read_json(_path):
        """
        Routine that reads a json file and returns its content as a Python dictionary.
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: Dictionary containing the content of the json file.
        """
        _dictionary = {}
        json_path = Path(_path)
        if json_path.is_file():
            with open(_path, "r") as json_file:
                _dictionary = json.load(json_file)

        return _dictionary

    @staticmethod
    def write_xml(_dict, _xml_path):
        """
        Writes a dictionary to a xml file.
        :param _dict: Dictionary containing key-value pairs.
        :param _xml_path: String containing a path to a file that should be written.
        :return: None. (Writes xml file directly.)
        """
        xml = dicttoxml(_dict)
        xml_file = open(_xml_path, "w")
        xml_file.write(xml.decode("utf-8"))
        xml_file.close()

        pass

    @staticmethod
    def write_html(_xml_path, _xsl_path):
        """
        Converts a xml string to a html file.
        :param _xml_path: String containing a path to the .xml file that was written.
        :param _xsl_path: String containing a path to the .xslt style sheet that should be used to create the html file.
        :return: String containing the name of the html file.
        """
        # parse xml to html using style sheet xslt
        xslt_doc = etree.parse(_xsl_path)
        xslt_transformer = etree.XSLT(xslt_doc)

        source_doc = etree.parse(_xml_path)
        output_doc = xslt_transformer(source_doc)

        output_html_path = _xml_path[:-4] + ".html"
        output_doc.write(output_html_path, pretty_print=True)

        return output_html_path.rsplit(os.path.sep, 1)[1]

    def revise_content(self, key, entry_dict, revised_entry_dict, extracted_entry_dict, eln_folder_path):
        """
        Decides whether content of the logbook is added to the revised_entry_dict or the extracted_entry_dict
        dictionary.
        :param key: String containing the key of the entry_dict dictionary whose value should be revised (i.e. removed
        from the final dictionary revised_entry_dict).
        :param entry_dict: Dictionary containing the original content for a given entry (at a given time).
        :param revised_entry_dict: Dictionary containing the cleaned content of the original entry that is to be
        published.
        :param extracted_entry_dict: Dictionary containing the part that was removed from the original content
        (i.e. the difference between original and revised entry).
        :return: None. Dictionaries revised_entry_dict and extracted_entry_dict are directly modified.
        """
        raw_content = entry_dict[key]
        if key == "TEXT":
            revised_content = ""
            extracted_content = ""
            # assuming that items are separated by '\n\n'
            separator = "\n\n"
            if isinstance(raw_content, str) and separator in raw_content:
                items = raw_content.split(separator)
                items_no = len(items)
                not_last_item = True
                for i in range(items_no):
                    if i == (items_no - 1):
                        not_last_item = False
                    item = items[i]
                    allowed = True
                    # allowed = False
                    # for element in self.text_whitelist:
                    #     if item.startswith(element):
                    #         allowed = True
                    #         break
                    if allowed:
                        revised_content += item
                    else:
                        revised_content += "(content removed)"
                        extracted_content += item
                        if not_last_item:
                            extracted_content += separator
                    if not_last_item:
                        revised_content += separator

                revised_entry_dict[key] = revised_content
                extracted_entry_dict[key] = extracted_content
        elif key == "ATTACHMENT":
            value_text = ""
            revised_image_list = []
            if "," in raw_content:
                image_list = raw_content.split(",")
            else:
                image_list = [raw_content]
            for image in image_list:
                image_path = os.path.join(eln_folder_path, image)
                # convert eps to png if required
                if image.endswith(".eps"):
                    project_dict = ConfigDict()
                    project_dict.read_json(project_meta_path())
                    if "ghostscript_path" in project_dict.get_content():
                        gs_path = project_dict.assemble_path("ghostscript_path")
                        EpsImagePlugin.gs_windows_binary = gs_path.encode('utf-8', 'ignore')
                        eps_image = Image.open(image_path)
                        eps_image.load(scale=10)
                        image_path = image_path.replace(".eps", ".png")
                        eps_image.save(image_path)
                if os.path.isfile(image_path):
                    revised_image_list.append(image_path)
            first_image = True
            for revised_image in revised_image_list:
                if first_image:
                    value_text = revised_image
                    first_image = False
                else:
                    value_text += "," + revised_image
            revised_entry_dict[key] = value_text
        elif key == "DATE":
            if entry_dict[key] is not None:
                date_log = entry_dict[key]
                date_iso = a4_date2iso(date_log)
                revised_entry_dict[key] = date_iso

        else:
            add_key = False
            if key not in self.blacklist:
                if entry_dict[key] is not None:
                    add_key = True

            if add_key:
                revised_entry_dict[key] = entry_dict[key]
            else:
                extracted_entry_dict[key] = entry_dict[key]

        pass

    @staticmethod
    def dict2pdf(_xml_path):
        """
        Reads an xml file and converts it into pdf file by applying a formatting provided by a xslt style sheet.
        :param _xml_path: String containing the path to the xml file that should be converted into pdf.
        :return: None. (Html and pdf files are directly produced.)
        """
        # parse xml to html using style sheet xslt
        working_dir = get_working_directory(__file__)
        style_sheet_path = os.path.join(working_dir, "config_eln", "style_sheet.xslt")
        xslt_doc = etree.parse(style_sheet_path)
        xslt_transformer = etree.XSLT(xslt_doc)

        source_doc = etree.parse(_xml_path)
        output_doc = xslt_transformer(source_doc)
        output_html_path = _xml_path[:-4] + ".html"
        output_doc.write(output_html_path, pretty_print=True)

        # write html to pdf using wkhtmltopdf
        output_pdf_path = _xml_path[:-4] + ".pdf"
        config = pdfkit.configuration(
            wkhtmltopdf="C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe"
        )
        pdfkit.from_file(output_html_path, output_pdf_path, configuration=config)

        # mimicking that an URL is given in the xml pointing to a picture (e.g. plot)
        url = (
            "https://www.helmholtz-berlin.de/media/media/forschung/wi/optik-strahlrohre/aquarius/"
            "usecasefluxresolutioncff2p25sv0p015.png"
        )
        # self.add_webpic2pdf(url, output_pdf_path)

        pass

    def add_webpic2pdf(self, picture_url, _pdf_path):
        """
        Reads an image file from a web URL, stores it locally in a file, converts it into a separate pdf and appends it
        to an existing pdf (i.e. the logbook pdf).
        :param picture_url: String containing the URL to a picture,
        e.g. 'https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg'
        :param _pdf_path: String containing the path to the pdf to which the picture should be appended, e.g. the
        logbook pdf.
        :return: None. The picture is directly appended to the pdf as an additional page.
        """
        # get image from web and save image locally
        image_data = requests.get(picture_url).content
        file_ending = picture_url.rsplit(".", 1)[1]
        filename = "example." + file_ending

        # create temp folder to write image
        working_dir = get_working_directory(__file__)
        folder_path = os.path.join(working_dir, "..", "..", "input_exampleData", "temp")
        create_path(folder_path)

        image_path = os.path.join(folder_path, filename)
        with open(image_path, "wb") as image_file:
            image_file.write(image_data)

        # convert picture to separate pdf
        dummy_pdf_path = _pdf_path[:-4] + "_image.pdf"
        pdf = FPDF("P", "mm", "A4")
        pdf.add_page()
        # pdf.image(image,x,y,w,h)
        pdf.image(image_path, 10, 10, 190, 110)
        pdf.output(dummy_pdf_path, "F")

        # append picture pdf to logbook pdf
        merged_pdf_path = _pdf_path[:-4] + "_pics_attended.pdf"
        pdfs = [_pdf_path, dummy_pdf_path]
        merger = PdfFileMerger()
        for pdf in pdfs:
            merger.append(pdf)

        merger.write(merged_pdf_path)
        merger.close()

        # delete intermediate files; keep only the last pdf version with pictures attended
        self.delete_file(_pdf_path)
        self.delete_file(dummy_pdf_path)

        pass

    @staticmethod
    def copy_images(output_path, image_path):
        """
        Copies all images of a folder to the ELN folder (irrespective of being mentioned in the ELN).
        :param output_path: String containing the target path to the ELN folder where images should be copied to.
        :param image_path: String containing the source path to the source folder where images should be copied from.
        :return: None.
        """
        files = os.listdir(image_path)
        for filename in files:
            shutil.copy2(os.path.join(image_path, filename), os.path.join(output_path, filename))
        pass

    @staticmethod
    def delete_file(_path):
        """
        (Note: Not moved to auxiliary/general_aux.py to be not available to other modules.)
        Deletes file if it exists, e.g. if it is an intermediate product for processing.
        :param _path: String containing the path to the file that has to be deleted.
        :return: None.
        """
        if Path(_path).is_file():
            os.remove(_path)

        pass


# standard entry point
if __name__ == "__main__":
    file_path = os.path.join(
        "D:", os.path.sep, "A4-metadata", "input_exampleData", "P2_testLogbook.xml"
    )
    # file_path = os.path.join('D:', os.path.sep, 'A4-metadata', 'input_exampleData', 'A4_elog_beam191010.xml')
    output_path = os.path.join(
        "D:", os.path.sep, "A4-metadata", "output", "P2_testLogbook_revised.xml"
    )
    eln = ELogConverter()
    eln.read_xml(file_path)
    eln_filename = eln.revise_logbook(output_path)
