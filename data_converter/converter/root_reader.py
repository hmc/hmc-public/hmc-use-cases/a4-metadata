# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

import datetime
import json
import os
import sys
from xml.etree import ElementTree as ET

import numpy as np
from lxml import etree

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

import ROOT
import uproot

# standard entry point
if __name__ == "__main__":
    file_path = os.path.join(
        "D:",
        os.path.sep,
        "a4-metadata",
        "input_exampleData",
        "A4_data_set",
        "medusa",
        "daten",
        "medusa_run78850.root",
    )
    with uproot.open(file_path) as file:
        print(file.keys())
        print(file.classnames())
        print(file["medusa.10"])
        medusa = file["medusa.10;1"]
        print(type(medusa))
        h = file["medusa.10"].Histo1D("Run 78850")

        # for key in file.keys():
        #    print(file[key].all_members)

        # for key, values in file['medusa.1'].items():
        #    print(key)

    data = uproot.open(file_path)["medusa.1"]
    data.show
