# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

import os
import shutil
import sys
from pathlib import Path

import numpy as np

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.config_dict import *
from auxiliary.general_aux import *
from xml_writer.xml_writer import *


class ASCIIConverter(object):
    """
    Processes ASCII files (e.g. copy, convert, or modify).
    """

    def __init__(self):
        """
        Initializes class variables.
        """
        self.raw_file_path = ""
        self.raw_file_name = ""
        self.output_path = ""
        self.file_dict = ConfigDict()
        self.config_dict = ConfigDict()

        pass

    def handle_file(self, _raw_file_path, parent_output_path, _output_path, _file_dict, _config_dict,
                    _readme_dict, report_dict):
        """
        Decides how to proceed with an ascii file.
        :param _raw_file_path: String containing the path to the original input file that should be processed,
        e.g. "D:/a4-metadata/input_exampleData/A4_data_set/run78850/medusa/daten/HighVoltages/voltages_run78850".
        :param parent_output_path: String containing the path to the data set folder to where the processed file should
        be written, e.g. "D:/a4-metadata/output/A4_data_set".
        :param _output_path: String containing the path to the folder to where the processed file should be written,
        e.g. "D:/a4-metadata/output/A4_data_set/run78850/medusa/daten/HighVoltages"
        :param _file_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the file that should be handled.
        :param _config_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the project that this file is part of.
        :param _readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (lower-level) readme file that should be written for each data set (which this file is part of).
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :return: None.
        """
        self.raw_file_path = _raw_file_path
        self.raw_file_name = _raw_file_path.rsplit(os.path.sep, 1)[1]
        self.output_path = _output_path
        self.file_dict.add_dict(_file_dict)
        self.config_dict.add_dict(_config_dict)

        _content_type = self.file_dict.get_value("content_type", self.raw_file_path)
        _convert_to = self.file_dict.get_value("convert_to", self.raw_file_path)

        run_description = ""
        if _convert_to == "xml":
            # start the corresponding xml writer depending on the defined content_type
            if _content_type == "table":
                project_name = parent_output_path.rsplit(os.path.sep, 1)[1]
                # xml_filename = self.table2xml(project_name, _file_dict.get_value("filename_part"), report_dict)
                xml_filename = self.table2xml(_file_dict.get_value("filename_part"), report_dict)

                # add converted file to readme dictionary
                _readme_dict.add_file(xml_filename, self.file_dict, parent_output_path, False, self.raw_file_name)

                # copy raw file to output folder and add it to readme dictionary
                raw_file_output_path = os.path.join(self.output_path, self.raw_file_name)
                self.copy_original(self.raw_file_path, raw_file_output_path, parent_output_path, True, xml_filename,
                                   "ASCII", _readme_dict, self.file_dict)

        pass

    def copy_original(self,
                      raw_file_path,
                      output_path,
                      parent_output_path,
                      original_file,
                      product_filename,
                      _format,
                      _readme_dict,
                      _file_dict):
        """
        :param raw_file_path: String containing the path (including the file name) where the output file should
        be written to.
        :param output_path: String containing the path to the folder to where the processed file should be written,
        e.g. "D:/a4-metadata/output/A4_data_set/run78850/medusa/daten/HighVoltages"
        :param parent_output_path: String containing the path to the data set folder to where the processed file should
        be written, e.g. "D:/a4-metadata/output/A4_data_set".
        :param original_file: Boolean that is True if the provided filename relates to a raw (not converted) source
        file, otherwise False; optional.
        :param product_filename: String containing the name of a generated file whose content is derived from that of
        the (raw) file of filename; optional, only used if raw_file_path belongs to a raw file.
        :param _format: String containing the name of the source file format, e.g. "ASCII".
        :param _readme_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the (lower-level) readme file that should be written for each data set (which this file is part of).
        :param _file_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing information about
        the file that should be handled.
        :return: None.
        """
        # copy raw file to output folder
        shutil.copy2(raw_file_path, output_path)

        # add raw file to readme dictionary
        _readme_dict.add_file(raw_file_path.rsplit(os.path.sep, 1)[1], _file_dict, parent_output_path, original_file,
                              product_filename, _format)
        pass

    def table2xml(self, file_part, report_dict):
        """
        Reads an ASCII table into a numpy array and writes it as an xml file.
        :param file_part: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :return: None. (Writes the xml file directly by calling the array2xml routine.)
        """
        if os.path.sep in self.raw_file_path:
            folder_path, filename = self.raw_file_path.rsplit(os.path.sep, 1)
        else:
            folder_path = ""
            filename = self.raw_file_path

        if find_file(folder_path, filename):
            with open(self.raw_file_path) as file:
                lines = file.readlines()

            corrected_lines = self.correct_table(lines, folder_path, report_dict)
            array_data = self.list2array(corrected_lines)

            # write array to xml file column-wise
            xml_filename = self.remove_ending(filename) + ".xml"
            xml_file = XMLWriter("resource", file_part)
            xml_file.create_header()
            xml_file.array2xml(array_data, self.file_dict, folder_path, report_dict)
            xml_file.write_xml(xml_filename, self.output_path)

            del xml_file

        return xml_filename

    @staticmethod
    def remove_ending(_filename):
        """
        Removes the extension of the file name and replaces other '.' by underlines.
        :param _filename: String containing the name of a file, e.g. 'Foersterlog_1288346262.xml'.
        :return: String (containing the filename without file ending, e.g. 'Foersterlog').
        """
        if "." in _filename:
            _filename = _filename.rsplit(".", 1)[0]
            _filename = _filename.replace(".", "_")

        return _filename

    def correct_table(self, list_of_lines, current_dir, report_dict):
        """
        Analyzes and corrects a table where possible. The table is expected to be passed as a list of (ASCII) rows, each
        containing the columns of the table separated by some whitespace (e.g. ' ' or tab).
        :param list_of_lines: List of strings which should correspond to the rows of the table; each line should contain
        the columns of the table.
        :param current_dir: String containing the path to the current folder.
        :param report_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing messages which
        detail the course of the automatic process.
        :return: List of strings corresponding to the (maybe corrected) rows of the table.
        """
        # get number of expected columns from file_dict (which contains config file 'subdata_meta.json')
        # expected_columns = len(self.file_dict.get_value("header", current_dir))
        expected_columns = self.get_expected_columns(current_dir)

        corrected_lines = []
        skip_line = False
        # going through the list, line by line
        for i in range(len(list_of_lines)):
            if skip_line:
                skip_line = False
                report_dict.add_merged_line(i, current_dir, self.raw_file_name)
            else:
                line = list_of_lines[i]
                # assume that header starts with '#'
                if not line.startswith("#"):
                    # 1. find separator: first check if whitespaces exists; if so, assume that they separate columns
                    columns, separator = self.get_columns(line)

                    # check if separator was found
                    if not separator == "":
                        found_column_no = len(columns)

                        # check if number of columns meets the expected number
                        if found_column_no == expected_columns:
                            # leave the line unchanged
                            corrected_lines.append(line)
                        else:
                            if found_column_no < expected_columns:
                                # check if there is an additional line break
                                if not i + 2 > len(list_of_lines):
                                    next_line = list_of_lines[i + 1]
                                    next_columns, separator = self.get_columns(next_line)
                                    if (
                                        len(next_columns) + found_column_no
                                    ) == expected_columns:
                                        corrected_lines.append(
                                            line.replace("\n", " ") + next_line
                                        )
                                        skip_line = True

        return corrected_lines

    def get_expected_columns(self, current_dir):
        """
        Searches file_dict for columns (including those in sub-tables) and returns their number.
        :param current_dir: String containing the path to the current folder.
        :return: Integer (containing number of columns as read from the json config part,
        e.g. of subdata_meta.json file).
        """
        if "expected_columns" in self.file_dict.get_keys():
            # indicates that sub-tables are included
            columns_found = int(self.file_dict.get_value("expected_columns", current_dir))
        else:
            # indicates that no sub-tables are expected
            header_dict = self.file_dict.get_value("header", current_dir)
            columns_found = len(header_dict.keys())

        return columns_found

    def list2array(self, _list):
        """
        Converts a list of strings each containing separated values to a 2-dimensional array
        of dimensions [list length], [number of values within string].
        :param _list: List of strings where each string contains separated values.
        :return: Numpy array of dimension [list length][number of values within string].
        """
        # get maximum column number
        max_columns = 0
        for line in _list:
            columns, _separator = self.get_columns(line)
            column_no = len(columns)
            if column_no > max_columns:
                max_columns = column_no

        # create array and fill with individual values (separated by _separator) of each line
        _array = np.zeros((len(_list), max_columns))
        for i in range(len(_list)):
            line = _list[i]
            columns, _separator = self.get_columns(line)
            if _separator == "whitespace":
                _array[i] = line.split()
            else:
                _array[i] = line.split(_separator)

        # return the inverted array (changing rows <-> columns)
        return _array.T

    @staticmethod
    def get_columns(_line, _separator=""):
        """
        Separates individual terms from a text string and returns them as a list; tries to identify the separator if
        none is provided.
        :param _line: String containing the content of a single line.
        :param _separator: String containing the separator between individual entries of the line,
        e.g. 'whitespace' (for all kind of whitespaces used by the 'split()' command) or ','.
        :return: Tuple of list (containing the columns of a single line) and string (containing the seperator).
        """
        # find separator if no separator is provided; first check if whitespaces exists and, if so, assume that they
        # separate columns
        if _separator == "":
            columns = _line.split()
            if len(columns) > 1:
                _separator = "whitespace"
            else:
                # otherwise, try other common separators
                common_sep_list = [",", ";"]
                for _sep in common_sep_list:
                    columns = _line.split(_sep)
                    if len(columns) > 1:
                        _separator = _sep
                        break
                # check for single column if no separator was found
                if not len(columns) == 0:
                    _separator = "whitespace"
        else:
            columns = _line.split(_separator)

        return columns, _separator
