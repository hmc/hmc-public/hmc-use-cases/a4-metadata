# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import datetime
import os
from pathlib import Path
import pytz


def get_working_directory(_file_path):
    """
    Creates an absolute path from the given path which, e.g. the current python script (__file__).
    :param _file_path: String containing a path to a folder or file.
    :return: String containing an absolute path.
    """
    file_path = Path(_file_path)
    return file_path.parent.absolute()


def create_path(_path):
    """
    Checks if a folder exists and, if not, creates the folder.
    :param _path: String containing a (relative) folder path, e.g. 'output/A4_data_set'
    :return: None
    """
    if not os.path.isdir(_path):
        path_segments = _path.split(os.path.sep)
        current_path = ""
        first_item = True
        for item in path_segments:
            if not first_item:
                current_path += os.path.sep
            current_path += item
            first_item = False
            if not os.path.isdir(current_path):
                os.makedirs(current_path)
    pass


def assemble_path(_origin_path, _path_segments):
    """
    Joins a list of path elements by adding the system separator.
    :param _origin_path: String containing the path to the origin from where _path_segments start, e.g. 'D:'.
    :param _path_segments: List of strings each containing a single path element (e.g. drive letter or folder), e.g.
    'a4_dataset/run78550'.
    :return: String containing the assembled path, e.g. 'D:/a4_dataset/run78550'.
    """
    target_path = _origin_path
    first_segment = True
    for segment in _path_segments:
        target_path = os.path.join(target_path, segment)
        if first_segment:
            # add additional separator if drive letter (e.g. 'C:')
            if segment.endswith(":"):
                target_path = os.path.join(target_path, os.path.sep)
            first_segment = False

    return target_path


def guess_file(folder_path, name_part):
    """
    Checks if 'filename' is part of a file's name in the given path and returns True or False as well as the
    file's name.
    :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
    :param name_part: String containing the name of a file or a part of it, e.g. 'logbook_whitelist.json'.
    :return: Tuple of boolean (that is 'True' if the file in the folder exists, otherwise 'False') and string
    (containing the file's name).
    """
    filename = ""
    file_found = False
    for file in os.listdir(folder_path):
        if name_part in file:
            file_found = True
            filename = file
            break
    return file_found, filename


def find_file(folder_path, filename):
    """
    Checks if a file in a given path exists and returns True or False.
    :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
    :param filename: String containing the name of a file, e.g. 'logbook_whitelist.json'.
    :return: Boolean that is 'True' if the file in the folder exists; otherwise 'False'.
    """
    file_found = False
    for file in os.listdir(folder_path):
        if file.startswith(filename):
            file_found = True
            break
    return file_found


def file2string(folder_path, filename, return_list=False):
    """
    Reads the content of a file as a string if return_list=False (default); otherwise, reads the content of a file as a
    list of strings (if return_list=True).
    :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
    :param filename: String containing the name of a file, e.g. 'logbook_whitelist.json'.
    :param return_list: Boolean which defines whether a file is read as a single list (False) or as a list of strings
    each containing a line (True); read as a single string is the default option.
    :return: String containing the content of the file _or_ a list of strings each containing a line of the file.
    """
    if return_list:
        text = []
    else:
        text = ""
    if find_file(folder_path, filename):
        # errors='ignore' suppresses errors due to strange characters (such as °) but may distort the content
        with open(
            os.path.join(folder_path, filename), "r", encoding="utf-8", errors="ignore"
        ) as f:
            for line in f:
                if return_list:
                    text.append(line)
                else:
                    text += line
    return text


def project_meta_path():
    """
    Returns the (absolute) path of the project_meta.json file.
    :return: String (containing the path to the project_meta.json file).
    """
    # read project information
    file_path = os.path.join(
        config_project_path(), "project_meta.json"
    )

    return file_path


def subdata_meta_path():
    """
    Returns the (absolute) path of the subdata_meta.json file.
    :return: String (containing the path to the project_meta.json file).
    """
    # read project information
    file_path = os.path.join(
        config_project_path(), "subdata_meta.json"
    )

    return file_path


def config_project_path():
    """
    Returns the (absolute) path of the config_project folder.
    :return: String (containing the path to the project_meta.json file).
    """
    # read project information
    working_dir = get_working_directory(__file__)
    folder_path = os.path.join(
        working_dir, "..", "config_project"
    )

    return folder_path


def time2utc(date_string):
    """
    Adds time shift to date of format 'YYYY-MM-DDThh:mm:ss',
    e.g. '2022-01-03T12:23:00' becomes '2022-01-03T12:23:00+01:00'.
    :param date_string: String containing the date in the format 'YYYY-MM-DDThh:mm:ss', e.g. '2022-01-03T12:23:00'.
    :return: String (containing the date in UTC format, e.g. '2022-01-03T12:23:00+01:00').
    """
    time_string = ""
    _date = date_string.strip().split("T")
    if len(_date) > 1:
        date_part = _date[0].split("-")
        if len(date_part) > 2:
            year = date_part[0]
            month = date_part[1]
            day = date_part[2]
            time_part = _date[1].split(":")
            if len(time_part) > 2:
                hour = time_part[0]
                minute = time_part[1]
                second = time_part[2]
                time_string = get_utc(datetime.datetime(int(year), int(month), int(day),
                                                        int(hour), int(minute), int(second)))
    return time_string


def get_utc(dt):
    """
    Converts a local datetime object into a string representing the UTC time with corresponding timezone shift; includes
    summer and winter time shift.
    :param dt: Datetime object representing a local time.
    :return: String (representing UTC time).
    """
    timezone = pytz.timezone("Europe/Berlin")
    timezone_date = timezone.localize(dt, is_dst=None)
    part = str(timezone_date).split("+")
    utc_shift = part[1]
    time = timezone_date.strftime("%Y-%m-%dT%H:%M:%S") + "+" + utc_shift
    return str(time)


def month2dig(month_str):
    """
    Translates the name of a month to its representation as a number, e.g. 'Feb' becomes '02'.
    :param month_str: String containing the name of a month or its abbreviation, e.g. 'October' or 'Oct'.
    :return: String (containing the number representation, e.g. '02').
    """
    dig_month = ""
    if month_str.endswith('.'):
        month_str = month_str[:-1]
    if month_str == "Jan" or month_str == "January":
        dig_month = "01"
    elif month_str == "Feb" or month_str == "February":
        dig_month = "02"
    elif month_str == "Mar" or month_str == "March":
        dig_month = "03"
    elif month_str == "Apr" or month_str == "April":
        dig_month = "04"
    elif month_str == "May" or month_str == "May":
        dig_month = "05"
    elif month_str == "Jun" or month_str == "June":
        dig_month = "06"
    elif month_str == "Jul" or month_str == "July":
        dig_month = "07"
    elif month_str == "Aug" or month_str == "August":
        dig_month = "08"
    elif month_str == "Sep" or month_str == "September":
        dig_month = "09"
    elif month_str == "Oct" or month_str == "October":
        dig_month = "10"
    elif month_str == "Nov" or month_str == "November":
        dig_month = "11"
    elif month_str == "Dec" or month_str == "December":
        dig_month = "12"
    return dig_month
