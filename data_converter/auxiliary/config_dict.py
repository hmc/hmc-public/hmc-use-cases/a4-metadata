# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

from dicttoxml import *
import importlib
import json
import os
import sys
from pathlib import Path
import xmltodict

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))

from auxiliary.general_aux import *


class ConfigDict(object):
    """
    Stores and provides content of a dictionary which is usually 1) build from a json file and 2) used to configure the
    data converter.
    """

    def __init__(self):
        """
        Initializes a dictionary.
        """
        self.dictionary = dict()

        pass

    def read_dict(self, _dict):
        """
        Assigns another dictionary to self.dictionary (overwriting its content).
        :param _dict: Dictionary containing key-value pairs.
        :return: None.
        """
        self.dictionary = _dict

        pass

    def read_json(self, _path):
        """
        Routine that reads a json file to the dictionary.
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: None.
        """
        json_path = Path(_path)
        if json_path.is_file():
            with open(_path, "r") as json_file:
                self.dictionary = json.load(json_file)

        pass

    def read_xml(self, xml_path):
        """
        Reads a xml file to the dictionary.
        :param xml_path: String containing the path to a xml file (including the filename),
        e.g. 'D:\A4-metadata\input_exampleData\logbook_whitelist.json'.
        :return: None.
        """
        folder_path, filename = xml_path.rsplit(os.path.sep, 1)
        file_text = file2string(folder_path, filename)
        if not file_text == "":
            self.dictionary = xmltodict.parse(file_text)

        pass

    def add_value(self, _key, _value):
        """
        Adds a value to the dictionary assigning it to the given key.
        :param _key: String containing the key of the dictionary to be looked for.
        :param _value: Any type which will be assigned to the _key.
        :return: None.
        """
        self.dictionary[_key] = _value

        pass

    def add_to_list(self, _key, _value):
        """
        Adds a value to a list that is assigned to the given key of the dictionary.
        :param _key: String containing the key that is assigned to a value of type list.
        :param _value: Any type which will be assigned to the list assigned to _key.
        :return: None.
        """
        # create list if not existing
        if _key not in self.dictionary:
            self.dictionary[_key] = []
        # add value to list
        self.dictionary[_key].append(_value)

        pass

    def add_to_report(self, _file_id, _category, _entry, _path):
        """
        Appends a text string (_entry) and a path string as a dictionary to a list that is assigned to
        [_file_id][_category] level of the report dictionary.
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :param _category: String containing the name of the category where the entry should be added,
        e.g. 'missing_definitions'.
        :param _entry: String containing the message that is to be written to the project log file.
        :param _path: String containing the path to the folder of the file that the current entry belongs to.
        :return: None.
        """
        if _file_id not in self.dictionary:
            cat_dict = {_category: []}
            self.dictionary[_file_id] = cat_dict
        else:
            if _category not in self.dictionary[_file_id]:
                self.dictionary[_file_id][_category] = []
        # add _entry if it doesn't already exist
        if _entry not in self.dictionary[_file_id][_category]:
            self.dictionary[_file_id][_category].append({"entry": _entry, "path": os.path.join(_path, _file_id)})

        pass

    def get_value(self, _key, _current_dir="", _readme_dict={}, _dict={}):
        """
        Reads the value of a key and returns that value if it exists. If value starts with "customClass:", the routine
        start_custom_class tries to execute a customized code of the following name which is expected to deliver the
        value to be returned.
        :param _key: String containing the key of the dictionary to be looked for.
        :param _current_dir: String containing the path to the current directory, e.g. pointing to the current run
        folder; optional, required for customized classes to localize themselves.
        :param _readme_dict: Handle to the sub-level readme dictionary to allow customized classes to add file
        information; optional.
        :param _dict: Dictionary to provide additional information for customized classes; optional.
        :return: Any type (containing the value of the key-value pair) or an empty string if key doesn't exist.
        """
        if _key in self.dictionary:
            if isinstance(self.dictionary[_key], str):
                key_word = "customClass:"
                if self.dictionary[_key].startswith(key_word):
                    custom_class_name = self.dictionary[_key].replace(key_word, "")
                    _value = self.start_custom_class(custom_class_name, _current_dir, _readme_dict, _dict)
                else:
                    _value = self.dictionary[_key]
            else:
                _value = self.dictionary[_key]
        else:
            _value = ""

        return _value

    @staticmethod
    def start_custom_class(custom_class_name, _current_dir, _readme_dict={}, _dict={}):
        """
        Initializes the class 'custom_class' and starts the method 'start()' which is expected to deliver the value
        which is returned. The class 'custom_class' must be in a python file of the same name (custom_class+'.py') in
        the folder './config_project'.
        :param custom_class_name: String containing the name of the class to be initialized.
        :param _current_dir: String containing the path to the current directory, e.g. pointing to the current run
        folder; required for customized classes to orientate themselves.
        :param _readme_dict: Handle to the sub-level readme dictionary to allow customized classes to add file
        information; optional.
        :param _dict: Dictionary to provide additional information for customized classes; optional.
        :return: Any type (depends on the returned value of the method custom_class.start).
        """
        value_assigned = False
        if not custom_class_name == '':
            if 1 == 1:
            #try:
                # import custom routine which is expected to be in folder /config_project/
                writer_import = 'config_project.custom_classes.' + custom_class_name
                writer_module = importlib.import_module(writer_import)
                globals().update(writer_module.__dict__)
                parameter_string = '(_current_dir, _readme_dict, _dict)'

                # call instrument's writer instance
                custom_class = eval('writer_module. ' + custom_class_name + parameter_string)
                _value = custom_class.start()

                # delete instance to release memory
                del custom_class

                value_assigned = True
            # except:
            #     pass

        if not value_assigned:
            _value = ""

        return _value

    def assemble_path(self, key):
        """
        Calls the routine confirm_assemble_dict_path and abstracts the boolean to give the resembled path without
        confirmation.
        :param key: String defining the dictionary key word that contains a list of path elements.
        :return: String resembling the path.
        """
        dummy, _path = self.confirm_assemble_path(key)
        return _path

    def confirm_assemble_path(self, key):
        """
        Assembles path from list of directories and adds additional path separators where necessary.
        :param key: String defining the dictionary key word that contains a list of path elements.
        :return: Tuple of boolean (that is True if path were assembled) and string containing the path.
        """
        path_found = False
        assembled_path = ""
        if key in self.dictionary:
            path_list = self.dictionary[key]
            for i in range(0, len(path_list)):
                element = path_list[i]
                if i > 0:
                    assembled_path = os.path.join(assembled_path, element)
                else:
                    if element.endswith(":"):
                        assembled_path = os.path.join(element, os.path.sep)
                    elif "." in element:
                        # assumes it is a network address such as 'basisit.de'
                        assembled_path = os.path.join(os.path.sep + os.path.sep, element)
                    else:
                        assembled_path = element
                path_found = True

        return path_found, assembled_path

    def get_items(self):
        """
        Returns the list of key-value pairs of the dictionary - as one would get with .items().
        :return: List of key-value pairs.
        """
        return self.dictionary.items()

    def get_keys(self):
        """
        Returns the list of keys of the dictionary - as one would get with .keys().
        :return: List of dictionary keys.
        """
        return self.dictionary.keys()

    def get_lowercase_keys(self):
        """
        Returns the list of keys in lowercase of the dictionary.
        :return: List (of dictionary keys in lowercase).
        """
        low_dict = {}
        for key, value in self.dictionary.items():
            low_dict[key.lower()] = value

        return low_dict

    def get_content(self):
        """
        Returns the content of the dictionary.
        :return: Dictionary.
        """
        return self.dictionary

    def write_xml(self, _xml_path):
        """
        Writes the dictionary to a xml file.
        :param _xml_path: String containing a path to a file that should be written.
        :return: None. (Writes xml file directly.)
        """
        xml = dicttoxml(self.dictionary)
        xml_file = open(_xml_path, "w")
        xml_file.write(xml.decode("utf-8"))
        xml_file.close()

        pass

    def add_file(self, filename, file_dict, _current_dir, original_file=False, related_filename="", _format=""):
        """
        Adds a filename and its relative path to the README dictionary ('landing page').
        :param filename: String containing the name of the file.
        :param file_dict: Dictionary containing information about the file as read from 'config/ascii_metadata.json'
        file.
        :param _current_dir: String containing the path to the current folder.
        :param original_file: Boolean that is True if the provided filename relates to a raw (not converted) source
        file, otherwise False; optional.
        :param related_filename: String containing the name of a related file, i.e. the source file if the current file
        is derived from that or the produced file if the current file is the original one.
        the (raw) file of filename; optional, only used if filename belongs to a raw file.
        :param _format: String containing the name of the source file format, e.g. "ASCII".
        :return: None.
        """
        if "datasets" not in self.dictionary:
            self.dictionary["datasets"] = []
        data_dict = {}
        data_dict["name"] = filename
        data_dict["output_path"] = file_dict.get_value("output_path", _current_dir)
        if not original_file:
            if "description" in file_dict.get_content():
                data_dict["description"] = file_dict.get_value("description", _current_dir)
            data_dict["prov"] = assemble_path("", data_dict["output_path"])
            data_dict["primarySource"] = related_filename
            data_dict["generatedBy"] = "data converter"
        else:
            data_dict["description"] = "Source data"
            if not _format == "":
                data_dict["description"] += " (" + str(_format) + ")"
            data_dict["description"] += " of " + related_filename + "."
            data_dict["generatedBy"] = "A4 experiment"

        if "footnote" in file_dict.get_content():
            data_dict["footnote"] = file_dict.get_value("footnote", _current_dir)

        self.add_to_list("datasets", data_dict)

        pass

    def add_abstract(self, _abstract_string):
        """
        Adds an abstract to the README dictionary.
        :param _abstract_string: String containing an abstract to be added to the README page.
        :return: None.
        """
        self.add_value("abstract", str(_abstract_string))

        pass

    def add_title(self, _title_string):
        """
        Adds the title of the README page to the dictionary.
        :param _title_string: String containing a title for the dataset.
        :return: None.
        """
        self.add_value("title", str(_title_string))

        pass

    def add_creation_date(self, _date_string):
        """
        Adds the measurement date to the dictionary of the README page.
        :param _date_string: String containing the measurement date in form of 'start_date/end_date'.
        :return: None.
        """
        self.add_value("creation_date", str(_date_string))

        pass

    def add_dict(self, _dict, _key=""):
        """
        Adds a single key-value pair or all key-value pairs (if no key is passed) of the dictionary-like class
        ConfigDict to the (class) self.dictionary.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs.
        :param _key: String containing the name of the key that should be added; optional; the entire dictionary is
        added if no key is passed.
        :return: None.
        """
        if _key == "":
            # add entire dictionary
            for _key, _value in _dict.get_items():
                self.dictionary[_key] = _value
        else:
            # add only single key-value pair
            if _key in _dict.get_content():
                self.dictionary[_key] = _dict.get_value(_key)

        pass
